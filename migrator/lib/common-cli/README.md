# Apache Commons CLI library
The Apache Commons CLI library provides an API for parsing command line options passed to programs. 
It's also able to print help messages detailing the options available for a command line tool. 

**Official website:** https://commons.apache.org/proper/commons-cli/

# License
[Apache License, version 2.0](https://www.apache.org/licenses/LICENSE-2.0)

# Artifacts
The library artifact is downloaded from https://repo1.maven.org/maven2/commons-cli/commons-cli/1.5.0/
