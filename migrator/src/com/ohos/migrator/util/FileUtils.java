/*
 * Copyright (c) 2022-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ohos.migrator.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class FileUtils {
    public static char[] readFileToCharArray(File file) throws IOException {
        try (FileReader fileReader = new FileReader(file);
            BufferedReader bufReader = new BufferedReader(fileReader)) {

            int length = (int)file.length(); // It's assumed a source file length fits into 2 GB.

            char[] buf = new char[length];
            int offset = 0;
            int left = length;
            int numRead;
            while ((numRead = bufReader.read(buf, offset, left)) != -1 && left > 0) {
                offset += numRead;
                left -= numRead;
            }

            return buf;
        } catch (Throwable t) {
            throw new IOException("Failed to read source file " + file.getPath());
        }
    }

    public static boolean textuallyEqual(File resultFile, File expectedFile) {
       String[] resultText = readFile(resultFile);
       String[] expectedText = readFile(expectedFile);

       // Temporary changes to offset for leading copyright & license comment.
       // TODO: Remove after comment translation is implemented!!!
       if (expectedText.length - resultText.length != 14)
       //if (resultText.length != expectedText.length)
           return false;

       for (int i = 0; i < resultText.length; ++i) {
           if (!resultText[i].equals(expectedText[i+14])) // TODO: Remove offset here when comments are translated!!!
               return false;
       }

       return true;
    }

    public static String[] readFile(File file) {
       List<String> result = new ArrayList<>();
       try (BufferedReader br = new BufferedReader(new FileReader(file))) {
           String line;
           while ((line = br.readLine()) != null) {
               // drop leading and trailing space
               // and empty lines
               line = line.trim();
               if (!line.isEmpty())
                   result.add(line);
           }
       }
       catch (IOException ioe) {
           System.err.println("Failed to read file " + file.getPath());
       }

       return result.toArray(new String[0]);
    }
}
