/*
 * Copyright (c) 2022-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ohos.migrator.staticTS;

import com.ohos.migrator.staticTS.parser.StaticTSParser;
import com.ohos.migrator.staticTS.parser.StaticTSParser.*;
import org.antlr.v4.runtime.CommonToken;
import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.Vocabulary;
import org.antlr.v4.runtime.tree.TerminalNode;
import org.antlr.v4.runtime.tree.TerminalNodeImpl;
import org.eclipse.jdt.core.dom.*;

public class NodeBuilder {
    private static final Vocabulary vocabulary = StaticTSParser.VOCABULARY;

    public static TerminalNode terminalNode(int type) {
        return new TerminalNodeImpl(new CommonToken(type, stsName(type)));
    }
    public static String stsName(int type) {
        // Antlr store all literal names wrapped into single quotes. Like: "'&='", "'^='", "'|='", "'=>'", "'null'", null,
        // "'do'", "'instanceof'", "'typeof'", "'case'",
        // Some values are null (for some codes/types).
        String stsName = vocabulary.getLiteralName(type);
        if (stsName == null) {
            assert(false);
            stsName = " <null> ";
        }
        else {
            assert(stsName.length() > 2);
            stsName = stsName.substring(1, stsName.length()-1);
        }

        return stsName;
    }

    public static TerminalNode terminalIdentifier(String identifier) {
        return new TerminalNodeImpl(new CommonToken(StaticTSParser.Identifier, identifier));
    }

    public static TerminalNode multiLineComment(String comment) {
        return new TerminalNodeImpl(new CommonToken(StaticTSParser.MultiLineComment, comment));
    }
    public static TerminalNode terminalIdentifier(SimpleName name) {
        return terminalIdentifier(name.getIdentifier());
    }

    // The 'name' here is expected to be a dot-separated sequence of names: 'name' '.' 'name' '.' 'name' ...
    public static QualifiedNameContext qualifiedName(String fqname) {
        QualifiedNameContext stsQualifiedName = new QualifiedNameContext(null, 0);
        stsQualifiedName.addChild(terminalIdentifier(fqname)).setParent(stsQualifiedName);
        return stsQualifiedName;
    }

    public static QualifiedNameContext qualifiedName(Name javaName) {
        return qualifiedName(javaName.getFullyQualifiedName());
    }

   private static int stsTypeNameCode(PrimitiveType.Code javaPrimitiveTypeCode) {
       int stsTypeNameCode = -1;

       if (javaPrimitiveTypeCode == PrimitiveType.BOOLEAN)
           stsTypeNameCode = StaticTSParser.Boolean;
       else if (javaPrimitiveTypeCode == PrimitiveType.BYTE)
           stsTypeNameCode = StaticTSParser.Byte;
       else if (javaPrimitiveTypeCode == PrimitiveType.CHAR)
           stsTypeNameCode = StaticTSParser.Char;
       else if (javaPrimitiveTypeCode == PrimitiveType.INT)
           stsTypeNameCode = StaticTSParser.Int;
       else if (javaPrimitiveTypeCode == PrimitiveType.DOUBLE)
           stsTypeNameCode = StaticTSParser.Double;
       else if (javaPrimitiveTypeCode == PrimitiveType.FLOAT)
           stsTypeNameCode = StaticTSParser.Float;
       else if (javaPrimitiveTypeCode == PrimitiveType.LONG)
           stsTypeNameCode = StaticTSParser.Long;
       else if (javaPrimitiveTypeCode == PrimitiveType.SHORT)
           stsTypeNameCode = StaticTSParser.Short;
       else if (javaPrimitiveTypeCode == PrimitiveType.VOID)
           stsTypeNameCode = StaticTSParser.Void;
       else
           assert false : "Unknown type";

       return stsTypeNameCode;
   }

    public static PredefinedTypeContext predefinedType(PrimitiveType.Code javaPrimitiveTypeCode) {
        // predefinedType -> TerminalNode<TypeName>
        PredefinedTypeContext stsPredefinedType = new PredefinedTypeContext(null, 0);
        stsPredefinedType.addChild(terminalNode(stsTypeNameCode(javaPrimitiveTypeCode)));
        return stsPredefinedType;
    }

    public static AccessibilityModifierContext accessibilityModifier(int javaModifiers) {
        int stsModifierCode = -1;
        if ((javaModifiers & Modifier.PRIVATE) != 0)
            stsModifierCode = StaticTSParser.Private;
        else if ((javaModifiers & Modifier.PROTECTED) != 0)
            stsModifierCode = StaticTSParser.Protected;
        else if ((javaModifiers & Modifier.PUBLIC) != 0)
            stsModifierCode = StaticTSParser.Public;

        if (stsModifierCode == -1) return null;

        AccessibilityModifierContext stsAccessMod = new AccessibilityModifierContext(null, 0);
        stsAccessMod.addChild(terminalNode(stsModifierCode));
        return stsAccessMod;
    }

    // STS tree:
    //      singleExpression: | literal  # LiteralExpression
    //      literal: NullLiteral
    public static SingleExpressionContext nullLiteral() {
        SingleExpressionContext stsExpression = new SingleExpressionContext(null, 0);
        LiteralContext stsLiteral = new LiteralContext(stsExpression, 0);
        stsLiteral.addChild(terminalNode(StaticTSParser.NullLiteral));
        stsExpression.addChild(stsLiteral).setParent(stsExpression);

        return stsExpression;
    }

    // STS tree:
    //      singleExpression: | literal  # LiteralExpression
    //      literal: | BooleanLiteral
    public static SingleExpressionContext boolLiteral(Boolean value) {
        SingleExpressionContext stsExpression = new SingleExpressionContext(null, 0);
        LiteralContext stsLiteral = new LiteralContext(null, 0);
        stsLiteral.addChild(new TerminalNodeImpl(new CommonToken(StaticTSParser.BooleanLiteral, value? "true" : "false")));
        stsExpression.addChild(stsLiteral).setParent(stsExpression);

        return stsExpression;
    }

    // STS tree:
    //      singleExpression: | literal  # LiteralExpression
    //      literal: | CharLiteral
    public static SingleExpressionContext charLiteral(String value) {
        // Add leading and/or terminating quotes if missing
        if (!value.startsWith("'")) value = "'" + value;
        if (!value.endsWith("'")) value += "'";

        SingleExpressionContext stsExpression = new SingleExpressionContext(null, 0);
        LiteralContext stsLiteral = new LiteralContext(null, 0);
        stsLiteral.addChild(new TerminalNodeImpl(new CommonToken(StaticTSParser.CharLiteral, value)));
        stsExpression.addChild(stsLiteral).setParent(stsExpression);

        return stsExpression;
    }

    // STS tree:
    //      singleExpression: | literal  # LiteralExpression
    //      literal: | StringLiteral
    public static SingleExpressionContext stringLiteral(String value) {
        // Add leading and/or terminating quotes if missing
        if (!value.startsWith("\"")) value = "\"" + value;
        if (!value.endsWith("\"")) value += "\"";

        // TODO: Escape all unescaped characters
        SingleExpressionContext stsExpression = new SingleExpressionContext(null, 0);
        LiteralContext stsLiteral = new LiteralContext(null, 0);
        stsLiteral.addChild(new TerminalNodeImpl(new CommonToken(StaticTSParser.StringLiteral, value)));
        stsExpression.addChild(stsLiteral).setParent(stsExpression);

        return stsExpression;
    }

    // STS tree:
    //      singleExpression: | literal  # LiteralExpression
    //      literal: | numericLiteral
    //      numericLiteral:
    //              : DecimalLiteral
    //              | HexIntegerLiteral
    //              | OctalIntegerLiteral
    //              | BinaryIntegerLiteral
    public static SingleExpressionContext numericLiteral(String value) {
        SingleExpressionContext stsExpression = new SingleExpressionContext(null, 0);
        LiteralContext stsLiteral = new LiteralContext(null, 0);

        CommonToken token;

        // parse string representation to create appropriate token
        // Ignore d and l suffices that Java allows for numeric literals
        // NOTE: The f suffix that Java also allows will be dealt with
        // later, as it might conflict with hexadecimal literals.
        if (value.endsWith("d") || value.endsWith("D") ||
            value.endsWith("l") || value.endsWith("L")) {
            value = value.substring(0, value.length() - 1);
        }

        if (value.startsWith("0b") || value.startsWith("0B")) {
            token = new CommonToken(StaticTSParser.BinaryIntegerLiteral, value);
        }
        else if (value.startsWith("0x") || value.startsWith("0X")) {
            token = new CommonToken(StaticTSParser.HexIntegerLiteral, value);
        }
        else if (value.startsWith("0") && value.length() > 1 &&
                !value.contains("89") && !value.contains(".")) {
            // STS octal literals start with 0o
            value = "0o" + value.substring(1);
            token = new CommonToken(StaticTSParser.OctalIntegerLiteral, value);
        }
        else {
            if (value.endsWith("f") || value.endsWith("F")) {
                value = value.substring(0, value.length() - 1);
            }
            token = new CommonToken(StaticTSParser.DecimalLiteral, value);
        }

        stsLiteral.addChild(new TerminalNodeImpl(token));
        stsExpression.addChild(stsLiteral).setParent(stsExpression);

        return stsExpression;
    }

    // Java:
    //   QualifiedType: Type . { Annotation } SimpleName
    //   SimpleType: { Annotation } TypeName
    // STS:
    //   typeReference: typeReferencePart ('.' typeReferencePart)*
    //   typeReferencePart: qualifiedName typeArguments?
    public static TypeReferenceContext typeReference(String typeName) {
        TypeReferenceContext stsTypeReference = new TypeReferenceContext(null, 0);
        TypeReferencePartContext stsTypeRefPart = typeReferencePart(typeName);
        stsTypeReference.addChild(stsTypeRefPart).setParent(stsTypeReference);
        return stsTypeReference;
    }

    public static TypeReferencePartContext typeReferencePart(String typeName) {
        TypeReferencePartContext stsTypeRefPart = new TypeReferencePartContext(null, 0);
        stsTypeRefPart.addChild(qualifiedName(typeName)).setParent(stsTypeRefPart);
        return stsTypeRefPart;
    }

    public static TypeReferenceContext typeReference(PrimitiveType javaPrimitivetype) {
        PrimitiveType.Code javaPrimitiveTypeCode = javaPrimitivetype.getPrimitiveTypeCode();
        TypeReferenceContext stsTypeReference = new TypeReferenceContext(null, 0);
        stsTypeReference.addChild(qualifiedName(stsName(stsTypeNameCode(javaPrimitiveTypeCode)))).setParent(stsTypeReference);
        return stsTypeReference;
    }
    public static TypeReferenceContext typeReference(String stsQualifierText, Name javaName) {
        String typeFQN = stsQualifierText + '.' + javaName.getFullyQualifiedName();

        TypeReferenceContext stsTypeReference = new TypeReferenceContext(null, 0);
        stsTypeReference.addChild(qualifiedName(typeFQN)).setParent(stsTypeReference);
        return stsTypeReference;
    }

    public static AssignmentOperatorContext assignmentOperator(Assignment.Operator javaAssignOp) {
        int stsOperatorCode = -1;

        if (javaAssignOp == Assignment.Operator.PLUS_ASSIGN)
            stsOperatorCode = StaticTSParser.PlusAssign;
        else if (javaAssignOp == Assignment.Operator.MINUS_ASSIGN)
            stsOperatorCode = StaticTSParser.MinusAssign;
        else if (javaAssignOp == Assignment.Operator.TIMES_ASSIGN)
            stsOperatorCode = StaticTSParser.MultiplyAssign;
        else if (javaAssignOp == Assignment.Operator.DIVIDE_ASSIGN)
            stsOperatorCode = StaticTSParser.DivideAssign;
        else if (javaAssignOp == Assignment.Operator.BIT_AND_ASSIGN)
            stsOperatorCode = StaticTSParser.BitAndAssign;
        else if (javaAssignOp == Assignment.Operator.BIT_OR_ASSIGN)
            stsOperatorCode = StaticTSParser.BitOrAssign;
        else if (javaAssignOp == Assignment.Operator.BIT_XOR_ASSIGN)
            stsOperatorCode = StaticTSParser.BitXorAssign;
        else if (javaAssignOp == Assignment.Operator.REMAINDER_ASSIGN)
            stsOperatorCode = StaticTSParser.ModulusAssign;
        else if (javaAssignOp == Assignment.Operator.LEFT_SHIFT_ASSIGN)
            stsOperatorCode = StaticTSParser.LeftShiftArithmeticAssign;
        else if (javaAssignOp == Assignment.Operator.RIGHT_SHIFT_SIGNED_ASSIGN)
            stsOperatorCode = StaticTSParser.RightShiftArithmeticAssign;
        else if (javaAssignOp == Assignment.Operator.RIGHT_SHIFT_UNSIGNED_ASSIGN)
            stsOperatorCode = StaticTSParser.RightShiftLogicalAssign;

        if (stsOperatorCode == -1) return null;

        AssignmentOperatorContext stsAssignOp = new AssignmentOperatorContext(null, 0);
        stsAssignOp.addChild(terminalNode(stsOperatorCode));
        return stsAssignOp;
    }

    // STS tree:
    //     singleExpression: | Identifier # IdentifierExpression
    public static SingleExpressionContext identifierExpression(String name) {
        SingleExpressionContext stsExpression = new SingleExpressionContext(null, 0);
        IdentifierExpressionContext stsIdentifier = new IdentifierExpressionContext(stsExpression);
        stsIdentifier.addChild(terminalIdentifier(name));
        stsExpression.addChild(stsIdentifier).setParent(stsExpression);

        return stsExpression;
    }

    public static void addExtraDimensions(ParserRuleContext stsCurrent, int extraDims) {
        assert(stsCurrent instanceof TypeAnnotationContext && extraDims > 0);

        PrimaryTypeContext stsPrimaryType = ((TypeAnnotationContext)stsCurrent).primaryType();
        ArrayTypeContext stsArrayType = stsPrimaryType.arrayType();
        if (stsArrayType == null) {
            // Should be either a type reference or predefined type.
            ParserRuleContext stsType = stsPrimaryType.typeReference();
            if (stsType == null) stsType = stsPrimaryType.predefinedType();
            assert(stsType != null);

            // Drop the type which we just extracted above.
            stsPrimaryType.removeLastChild();

            // Construct new ArrayTypeContext and link it up to PrimaryTypeContext.
            stsArrayType = new ArrayTypeContext(stsPrimaryType, 0);
            stsArrayType.addChild(stsType).setParent(stsArrayType);
            stsPrimaryType.addChild(stsArrayType);
        }

        for (int i = 0; i < extraDims; ++i) {
            stsArrayType.addChild(terminalNode(StaticTSParser.OpenBracket));
            stsArrayType.addChild(terminalNode(StaticTSParser.CloseBracket));
        }
    }

    public static ArrayTypeContext arrayType(String elementTypeName, int dimensions) {
        ArrayTypeContext stsArrayType = new ArrayTypeContext(null, 0);
        stsArrayType.addChild(typeReference(elementTypeName)).setParent(stsArrayType);
        for (int i = 0; i < dimensions; ++i) {
            stsArrayType.addChild(terminalNode(StaticTSParser.OpenBracket));
            stsArrayType.addChild(terminalNode(StaticTSParser.CloseBracket));
        }
        return stsArrayType;
    }

    public static TypeAnnotationContext typeAnnotation(TypeReferenceContext stsTypeRef) {
        TypeAnnotationContext stsTypeAnno = new TypeAnnotationContext(null, 0);
        PrimaryTypeContext stsPrimaryType = new PrimaryTypeContext(stsTypeAnno, 0);
        stsPrimaryType.addChild(stsTypeRef).setParent(stsPrimaryType);
        stsTypeAnno.addChild(stsPrimaryType).setParent(stsTypeAnno);
        return stsTypeAnno;
    }

    public static TypeAnnotationContext typeAnnotation(String stsTypeName) {
        TypeReferenceContext stsTypeRef = typeReference(stsTypeName);
        return typeAnnotation(stsTypeRef);
    }

    public static TypeAnnotationContext typeAnnotation(PredefinedTypeContext stsPredefType) {
        TypeAnnotationContext stsTypeAnno = new TypeAnnotationContext(null, 0);
        PrimaryTypeContext stsPrimaryType = new PrimaryTypeContext(stsTypeAnno, 0);
        stsPrimaryType.addChild(stsPredefType).setParent(stsPrimaryType);
        stsTypeAnno.addChild(stsPrimaryType).setParent(stsTypeAnno);
        return stsTypeAnno;
    }
    public static TypeAnnotationContext typeAnnotation(ArrayTypeContext stsArrayType) {
        TypeAnnotationContext stsTypeAnno = new TypeAnnotationContext(null, 0);
        PrimaryTypeContext stsPrimaryType = new PrimaryTypeContext(stsTypeAnno, 0);
        stsPrimaryType.addChild(stsArrayType).setParent(stsPrimaryType);
        stsTypeAnno.addChild(stsPrimaryType).setParent(stsTypeAnno);
        return stsTypeAnno;
    }

    public static ParameterContext parameter(String stsParamName, String stsParamType) {
        ParameterContext stsParam = new ParameterContext(null, 0);
        stsParam.addChild(terminalIdentifier(stsParamName));
        stsParam.addChild(typeAnnotation(stsParamType)).setParent(stsParam);
        return stsParam;
    }

    public static ParameterContext parameter(String stsParamName, PrimitiveType.Code javaPrimitiveTypeCode) {
        ParameterContext stsParam = new ParameterContext(null, 0);
        stsParam.addChild(terminalIdentifier(stsParamName));
        stsParam.addChild(typeAnnotation(predefinedType(javaPrimitiveTypeCode))).setParent(stsParam);
        return stsParam;
    }

    public static ConstructorCallContext ctorCall(boolean isSuperCall, String... stsArgNames) {
        ConstructorCallContext stsSuperCtorCall = new ConstructorCallContext(null, 0);
        stsSuperCtorCall.addChild(terminalNode(isSuperCall ? StaticTSParser.Super : StaticTSParser.This));

        ArgumentsContext stsSuperCtorCallArgs = new ArgumentsContext(stsSuperCtorCall, 0);
        stsSuperCtorCall.addChild(stsSuperCtorCallArgs).setParent(stsSuperCtorCall);
        ExpressionSequenceContext stsExprSeq = new ExpressionSequenceContext(stsSuperCtorCallArgs, 0);
        stsSuperCtorCallArgs.addChild(stsExprSeq).setParent(stsSuperCtorCallArgs);

        for (String stsArgName : stsArgNames) {
            stsExprSeq.addChild(identifierExpression(stsArgName)).setParent(stsExprSeq);
        }

        return stsSuperCtorCall;
    }

    public static TypeAnnotationContext unknownTypeAnnotation() {
        return typeAnnotation("__UnknownType__");
    }

    public static TypeAnnotationContext unknownTypeAnnotation(Type javaType) {
        TypeAnnotationContext stsTypeAnnotation = unknownTypeAnnotation();

        if (javaType != null) {
            stsTypeAnnotation.addChild(multiLineComment("/* " + javaType.toString() + " */"));
        }

        return stsTypeAnnotation;
    }

    public static TypeAnnotationContext unknownTypeAnnotation(ITypeBinding javaTypeBinding) {
        TypeAnnotationContext stsTypeAnnotation = unknownTypeAnnotation();

        if (javaTypeBinding != null) {
            stsTypeAnnotation.addChild(multiLineComment("/* " + javaTypeBinding.getName() + " */"));
        }

        return stsTypeAnnotation;
    }

    private static SingleExpressionContext dummyCall(String callName, String comment) {
        SingleExpressionContext stsExpression = new SingleExpressionContext(null, 0);
        CallExpressionContext stsCallExpression = new CallExpressionContext(stsExpression);
        stsExpression.addChild(stsCallExpression).setParent(stsExpression);

        SingleExpressionContext stsIdentifier = identifierExpression(callName);
        stsCallExpression.addChild(stsIdentifier).setParent(stsCallExpression);

        ArgumentsContext stsArguments = new ArgumentsContext(stsCallExpression, 0);
        stsCallExpression.addChild(stsArguments).setParent(stsCallExpression);

        stsArguments.addChild(multiLineComment("/* " + comment + " */"));

        return stsExpression;
    }

    public static SingleExpressionContext untranslatedExpression(ASTNode node) {
        return dummyCall("__untranslated_expression", node.toString());
    }

    public static boolean needStatementOrLocalDeclaration(ParserRuleContext stsContext) {
        return stsContext.getRuleIndex() == StaticTSParser.RULE_block
                || stsContext.getRuleIndex() == StaticTSParser.RULE_constructorBody;
    }

    public static ParserRuleContext untranslatedStatement(ASTNode node, ParserRuleContext stsContext) {
        StatementContext stsStatement = new StatementContext(null, 0);
        ExpressionStatementContext stsExprStatement = new ExpressionStatementContext(stsStatement, 0);
        stsStatement.addChild(stsExprStatement);
        stsExprStatement.addChild(dummyCall("__untranslated_statement", node.toString())).setParent(stsExprStatement);

        if (needStatementOrLocalDeclaration(stsContext)) {
            StatementOrLocalDeclarationContext stsStmtOrLocalDecl = new StatementOrLocalDeclarationContext(null, 0);
            stsStmtOrLocalDecl.addChild(stsStatement).setParent(stsStmtOrLocalDecl);
            return stsStmtOrLocalDecl;
        }

        return stsStatement;
    }

    public static ParserRuleContext untranslatedTryResource(ASTNode node, ParserRuleContext stsContext) {
        StatementContext stsStatement = new StatementContext(null, 0);
        ExpressionStatementContext stsExprStatement = new ExpressionStatementContext(stsStatement, 0);
        stsStatement.addChild(stsExprStatement);
        stsExprStatement.addChild(dummyCall("__untranslated_try_resource", node.toString())).setParent(stsExprStatement);

        if (needStatementOrLocalDeclaration(stsContext)) {
            StatementOrLocalDeclarationContext stsStmtOrLocalDecl = new StatementOrLocalDeclarationContext(null, 0);
            stsStmtOrLocalDecl.addChild(stsStatement).setParent(stsStmtOrLocalDecl);
            return stsStmtOrLocalDecl;
        }

        return stsStatement;
    }

    public static ShiftOperatorContext shiftOperator(InfixExpression.Operator javaOp) {
        ShiftOperatorContext stsShiftOp = new ShiftOperatorContext(null, 0);

        if (javaOp == InfixExpression.Operator.LEFT_SHIFT) {
            stsShiftOp.addChild(NodeBuilder.terminalNode(StaticTSParser.LessThan));
            stsShiftOp.addChild(NodeBuilder.terminalNode(StaticTSParser.LessThan));
        }
        else if (javaOp == InfixExpression.Operator.RIGHT_SHIFT_SIGNED) {
            stsShiftOp.addChild(NodeBuilder.terminalNode(StaticTSParser.MoreThan));
            stsShiftOp.addChild(NodeBuilder.terminalNode(StaticTSParser.MoreThan));
        }
        else if (javaOp == InfixExpression.Operator.RIGHT_SHIFT_UNSIGNED) {
            stsShiftOp.addChild(NodeBuilder.terminalNode(StaticTSParser.MoreThan));
            stsShiftOp.addChild(NodeBuilder.terminalNode(StaticTSParser.MoreThan));
            stsShiftOp.addChild(NodeBuilder.terminalNode(StaticTSParser.MoreThan));
        }

        return stsShiftOp;
    }

    public static void addArgument(CallExpressionContext stsCallExpr, SingleExpressionContext stsArg) {
        ArgumentsContext stsArgs = stsCallExpr.arguments();
        if (stsArgs != null) {
            ExpressionSequenceContext stsExprSeq = stsArgs.expressionSequence();
            if (stsExprSeq != null) {
                stsExprSeq.addChild(stsArg).setParent(stsExprSeq);
            }
        }
    }

    public static SingleExpressionContext thisExpression(TypeReferenceContext stsTypeRef) {
        SingleExpressionContext stsSingleExpr = new SingleExpressionContext(null, 0);
        ThisExpressionContext stsThisExpression = new ThisExpressionContext(stsSingleExpr);
        stsSingleExpr.addChild(stsThisExpression).setParent(stsSingleExpr);

        if (stsTypeRef != null) {
            stsThisExpression.addChild(stsTypeRef).setParent(stsThisExpression);
        }
        stsThisExpression.addChild(terminalNode(StaticTSParser.This));

        return stsSingleExpr;
    }

    public static SingleExpressionContext classLiteral(String className) {
        // Sanity check
        if (className == null) return null;

        SingleExpressionContext stsSingleExpr = new SingleExpressionContext(null, 0);
        ClassLiteralExpressionContext stsClassLiteral = new ClassLiteralExpressionContext(stsSingleExpr);
        stsSingleExpr.addChild(stsClassLiteral).setParent(stsSingleExpr);

        // NOTE: Class literal requires PrimaryTypeContext!
        //     | primaryType Dot Class      # ClassLiteralExpression
        PrimaryTypeContext stsPrimaryType = new PrimaryTypeContext(stsClassLiteral, 0);
        stsPrimaryType.addChild(typeReference(className)).setParent(stsPrimaryType);
        stsClassLiteral.addChild(stsPrimaryType).setParent(stsClassLiteral);

        stsClassLiteral.addChild(terminalNode(StaticTSParser.Dot));
        stsClassLiteral.addChild(terminalNode(StaticTSParser.Class));

        return stsSingleExpr;
    }
}
