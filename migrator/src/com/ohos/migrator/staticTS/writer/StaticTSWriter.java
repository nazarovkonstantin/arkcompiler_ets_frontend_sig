/*
 * Copyright (c) 2022-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ohos.migrator.staticTS.writer;

import com.ohos.migrator.staticTS.parser.StaticTSLexer;
import com.ohos.migrator.staticTS.parser.StaticTSParser;
import com.ohos.migrator.staticTS.parser.StaticTSParser.*;
import com.ohos.migrator.staticTS.parser.StaticTSParserBaseVisitor;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.tree.TerminalNode;

import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.List;

public class StaticTSWriter extends StaticTSParserBaseVisitor<Void> {
    private final Writer out;
    private final String indentStep = "    "; // 4 spaces
    private final StringBuffer sb = new StringBuffer();
    private String indentCurrent = "";

    public StaticTSWriter(Writer w) {
        out = w;
    }

    public void close() {
        if (out != null) {
            try {
                out.write(sb.toString());
                out.flush();
                out.close();
            } catch (IOException e) {
                System.out.print("Fail to flush and close the output file.");
            }
        }
    }

    public static void main(String[] args) {
        if (args.length >= 1) {
            try {
                CharStream input = CharStreams.fromFileName(args[0]);
                StaticTSLexer lexer = new StaticTSLexer(input);
                CommonTokenStream tokens = new CommonTokenStream(lexer);
                StaticTSParser parser = new StaticTSParser(tokens);

                CompilationUnitContext compilationUnit = parser.compilationUnit();

                if (args.length >= 2) {
                    try {
                        StaticTSWriter writer = new StaticTSWriter(new FileWriter(args[1]));
                        writer.visit(compilationUnit);
                        writer.close();
                    } catch (IOException e) {
                        System.out.printf("Fail to open the specified out file: \"%s\"", args[1]);
                    }
                }
            } catch (IOException e) {
                System.out.printf("Fail to open the specified input file: \"%s\"", args[0]);
            }
        }
        else
            System.out.println("The first argument to specify file to parse.");
    }

    private void indentIncrement() {
        indentCurrent += indentStep;
    }

    private void indentDecrement() {
        indentCurrent = indentCurrent.substring(0, indentCurrent.length() - indentStep.length());
    }

    // Return is a boolean value. Maybe sometime if indent is not inserted there will be need in a white space char.
    private boolean doNeededIndent() {
        if (sb.length() > 1 && sb.charAt(sb.length()-1) == '\n') {
            sb.append(indentCurrent);
            return true;
        }

        return false;
    }

    // initializer: '=' (arrayLiteral | singleExpression)
    @Override
    public Void visitInitializer(InitializerContext stsInitializer) {
        sb.append("= ");

        SingleExpressionContext stsExpression = stsInitializer.singleExpression();
        assert(stsExpression != null);
        stsExpression.accept(this);

        return null;
    }

    // typeParameters: '<' typeParameterList? '>'
    @Override
    public Void visitTypeParameters(TypeParametersContext stsTypeParameters) {
        sb.append('<');

        TypeParameterListContext stsTypeParameterList = stsTypeParameters.typeParameterList();
        if (stsTypeParameterList != null) {
            visitTypeParameterList(stsTypeParameterList);
        }

        sb.append('>');

        return null;
    }

    // typeParameterList: typeParameter (',' typeParameter)*;
    @Override
    public Void visitTypeParameterList(TypeParameterListContext stsTypeParameterList) {
        int i = 0;
        for (TypeParameterContext stsTypeParameter : stsTypeParameterList.typeParameter()) {
            if (i > 0) sb.append(", ");
            visitTypeParameter(stsTypeParameter);
            ++i;
        }

        return null;
    }

    // typeParameter: Identifier constraint? | typeParameters;
    @Override
    public Void visitTypeParameter(TypeParameterContext stsTypeParameter) {
        TerminalNode termIdentifier = stsTypeParameter.Identifier();
        sb.append(termIdentifier.getText());
        ConstraintContext stsConstraint = stsTypeParameter.constraint();
        if (stsConstraint != null) {
            sb.append(' ');
            visitConstraint(stsConstraint);
        }

        return null;
    }

    // constraint: 'extends' type_
    @Override
    public Void visitConstraint(ConstraintContext stsConstraint) {
        TerminalNode termExtends = stsConstraint.Extends();
        sb.append(termExtends.getText()).append(' ');

        TypeReferenceContext stsType = stsConstraint.typeReference();
        if (stsType != null) {
            visitTypeReference(stsType);
        }
        else {
            IntersectionTypeContext stsIntersectionType = stsConstraint.intersectionType();
            assert stsIntersectionType != null;
            visitIntersectionType(stsIntersectionType);
        }

        return null;
    }

    // typeArguments: '<' typeArgumentList? '>'
    @Override
    public Void visitTypeArguments(TypeArgumentsContext stsTypeArguments) {
        sb.append('<');

        TypeArgumentListContext stsTypeArgumentList = stsTypeArguments.typeArgumentList();
        if (stsTypeArgumentList != null) {
            visitTypeArgumentList(stsTypeArgumentList);
        }

        sb.append('>');

        return null;
    }

    // typeArgumentList: typeArgument (',' typeArgument)*
    @Override
    public Void visitTypeArgumentList(TypeArgumentListContext stsTypeArgumentList) {
        int i = 0;
        for (TypeArgumentContext stsTypeArgument : stsTypeArgumentList.typeArgument()) {
            if (i > 0) sb.append(", ");
            visitTypeArgument(stsTypeArgument);
            ++i;
        }

        return null;
    }

    // typeArgument: typeReference | arrayType | wildcardType
    @Override
    public Void visitTypeArgument(TypeArgumentContext stsTypeArgument) {
        TypeReferenceContext stsTypeReference = stsTypeArgument.typeReference();
        if (stsTypeReference != null) {
            visitTypeReference(stsTypeReference);
        }
        else {
            ArrayTypeContext staArrayType = stsTypeArgument.arrayType();
            if (staArrayType != null) {
                visitArrayType(stsTypeArgument.arrayType());
            }
            else {
                WildcardTypeContext stsWildcardType = stsTypeArgument.wildcardType();
                assert(stsWildcardType != null);
                visitWildcardType(stsWildcardType);
            }
        }

        return null;
    }

    // wildcardType: QuestionMark wildcardBound?
    @Override
    public Void visitWildcardType(WildcardTypeContext stsWildcardType) {
        sb.append('?');

        WildcardBoundContext stsWildcardBound = stsWildcardType.wildcardBound();
        if (stsWildcardBound != null) {
            sb.append(' ');
            visitWildcardBound(stsWildcardBound);
        }

        return null;
    }

    // wildcardBound: (Extends | Super) typeReference
    @Override
    public Void visitWildcardBound(WildcardBoundContext stsWildcardBound) {
        TerminalNode term = stsWildcardBound.Extends();
        if (term == null) term = stsWildcardBound.Super();

        assert(term != null);
        sb.append(term.getText()).append(' ');

        TypeReferenceContext stsTypeRef = stsWildcardBound.typeReference();
        assert(stsTypeRef != null);
        visitTypeReference(stsTypeRef);

        return null;
    }

    @Override
    public Void visitIntersectionType(IntersectionTypeContext stsIntersectionType) {
        List<TypeReferenceContext> stsTypeRefs = stsIntersectionType.typeReference();

        sb.append('(');

        for (int i = 0; i < stsTypeRefs.size(); ++i) {
            if (i > 0) sb.append(" & ");
            visitTypeReference(stsTypeRefs.get(i));
        }

        sb.append(')');
        return null;
    }

    // typeReference: typeReferencePard (Dot typeReferencePart)?
    @Override
    public Void visitTypeReference(TypeReferenceContext stsTypeReference) {
        List<TypeReferencePartContext> stsTypeParts = stsTypeReference.typeReferencePart();
        for (int i = 0; i < stsTypeParts.size(); ++i) {
            if (i > 0) sb.append('.');
            visitTypeReferencePart(stsTypeParts.get(i));
        }

        return null;
    }

    // typeReferencePart: qualifiedNmae typeArguments?
    @Override
    public Void visitTypeReferencePart(TypeReferencePartContext stsTypeReferencePart) {
        visitQualifiedName(stsTypeReferencePart.qualifiedName());

        TypeArgumentsContext ststTypeArguments = stsTypeReferencePart.typeArguments();
        if (ststTypeArguments != null) {
            visitTypeArguments(ststTypeArguments);
        }

        return null;
    }

    // qualifiedName: Identifier (Dot Identifier)*
    @Override
    public Void visitQualifiedName(QualifiedNameContext stsQualName) {
        int i = 0;
        for (TerminalNode identifier : stsQualName.Identifier()) {
            sb.append(identifier.getText());
            if (i > 0) sb.append('.');
            ++i;
        }

        return null;
    }

//    predefinedType
//    : Byte
//    | Short
//    | Int
//    | Long
//    | Float
//    | Double
//    | Boolean
//    | String
//    | Char
//    | Void
//    ;
    @Override
    public Void visitPredefinedType(PredefinedTypeContext stsPredefinedType) {
        sb.append(stsPredefinedType.getText());
        return null;
    }

    // arrayType: (predefinedType | typeReference) ('[' ']')+
    @Override
    public Void visitArrayType(ArrayTypeContext stsArrayType) {
        TypeReferenceContext stsTypeRef = stsArrayType.typeReference();
        if (stsTypeRef != null) {
            visitTypeReference(stsTypeRef);
        }
        else {
            PredefinedTypeContext stsPredefType = stsArrayType.predefinedType();
            assert(stsPredefType != null);
            visitPredefinedType(stsPredefType);
        }

        List<TerminalNode> openBrackets = stsArrayType.OpenBracket();
        List<TerminalNode> closeBrackets = stsArrayType.CloseBracket();
        int openBracketsNum = openBrackets.size();
        int closeBracketsNum = closeBrackets.size();
        assert(openBracketsNum > 0 && openBracketsNum == closeBracketsNum);
        for (int i = 0; i < openBracketsNum; ++i)
            sb.append("[]");

        return null;
    }

    // typeAnnotation: ':' primaryType
    @Override
    public Void visitTypeAnnotation(TypeAnnotationContext stsTypeAnnotation) {
        sb.append(": ");

        PrimaryTypeContext stsType = stsTypeAnnotation.primaryType();
        stsType.accept(this);

        if (stsTypeAnnotation.parent.getRuleIndex() != StaticTSParser.RULE_parameter &&
            stsTypeAnnotation.parent.getRuleIndex() != StaticTSParser.RULE_exceptionParameter)
            sb.append(' ');

        if (stsTypeAnnotation.getChildCount() > 1) {
            // In case of unresolved or invalid type, we add a comment to TypeAnnotationContext
            // node indicating the original type name where we can (the type itself is emitted as
            // __UnknownType__, see, e.g., JavaTransformer.createCatchOrRecoverClause method).
            // Here we check if that additional child of TypeAnnotationContext node exists,
            // and if so, emit it in STS source code.
            ParseTree lastChild = stsTypeAnnotation.getChild(stsTypeAnnotation.getChildCount()-1);
            if (lastChild instanceof TerminalNode) {
                stsTypeAnnotation.getChild(1).accept(this);
            }
        }

        return null;
    }

    // signature: typeParameters? '(' parameterList? ')' typeAnnotation
    @Override
    public Void visitSignature(SignatureContext stsSignature) {
        TypeParametersContext stsTypeParameters = stsSignature.typeParameters();
        if (stsTypeParameters != null) {
            visitTypeParameters(stsTypeParameters);
        }

        sb.append('(');

        ParameterListContext stsParameterList = stsSignature.parameterList();
        if (stsParameterList != null) {
            visitParameterList(stsParameterList);
        }

        sb.append(')');

        visitTypeAnnotation(stsSignature.typeAnnotation());

        return null;
    }


    // accessibilityModifier: Public | Private | Protected
    @Override
    public Void visitAccessibilityModifier(AccessibilityModifierContext stsAccessibilityModifier) {
        doNeededIndent();
        sb.append(stsAccessibilityModifier.getText()).append(' ');
        return null;
    }

    // constructorDeclaration: Constructor typeParameters? '(' parameterList? ')' constructorBody
    @Override
    public Void visitConstructorDeclaration(ConstructorDeclarationContext stsConstructorDeclaration) {
        doNeededIndent();

        sb.append(stsConstructorDeclaration.Constructor().getText());

        TypeParametersContext stsTypeParameters = stsConstructorDeclaration.typeParameters();
        if (stsTypeParameters != null) {
            visitTypeParameters(stsTypeParameters);
        }

        sb.append('(');

        ParameterListContext stsParameterList = stsConstructorDeclaration.parameterList();
        if (stsParameterList != null) {
            visitParameterList(stsParameterList);
        }

        sb.append(") {\n");

        indentIncrement();
        visitConstructorBody(stsConstructorDeclaration.constructorBody());
        indentDecrement();

        sb.append(indentCurrent).append("}\n\n");

        return null;
    }

    @Override
    public Void visitConstructorBody(ConstructorBodyContext stsConstructorBody) {
        ConstructorCallContext stsConstructorCall = stsConstructorBody.constructorCall();
        if (stsConstructorCall != null) visitConstructorCall(stsConstructorCall);

        for (StatementOrLocalDeclarationContext stsStatementOrLocalDecl : stsConstructorBody.statementOrLocalDeclaration())
            visitStatementOrLocalDeclaration(stsStatementOrLocalDecl);

        return null;
    }

    @Override
    public Void visitConstructorCall(ConstructorCallContext stsConstructorCall) {
        doNeededIndent();

        TerminalNode term = stsConstructorCall.Try();
        if (term != null) sb.append(term.getText()).append(' ');

        SingleExpressionContext stsSuperExpr = stsConstructorCall.singleExpression();
        if (stsSuperExpr != null) {
            stsSuperExpr.accept(this);
            sb.append('.');
        }

        term = stsConstructorCall.This();
        if (term == null) term = stsConstructorCall.Super();

        assert(term != null);
        sb.append(term.getText());

        TypeArgumentsContext stsTypeArguments = stsConstructorCall.typeArguments();
        if (stsTypeArguments != null) visitTypeArguments(stsTypeArguments);

        ArgumentsContext stsArguments = stsConstructorCall.arguments();
        assert(stsArguments != null);
        visitArguments(stsArguments);

        sb.append(";\n");

        return null;
    }

    // interfaceDeclaration: Export? Declare? Interface Identifier typeParameters? interfaceExtendsClause? '{' interfaceBody '}'
    // interfaceDeclaration: accessibilityModifier? Interface Identifier typeParameters? interfaceExtendsClause? '{' interfaceBody '}'
    @Override
    public Void visitInterfaceDeclaration(InterfaceDeclarationContext stsInterfaceDeclaration) {
        doNeededIndent();

        sb.append(stsInterfaceDeclaration.Interface().getText()).append(' ');
        sb.append(stsInterfaceDeclaration.Identifier().getText());

        TypeParametersContext stsTypeParameters = stsInterfaceDeclaration.typeParameters();
        if (stsTypeParameters != null) {
            visitTypeParameters(stsTypeParameters);
        }

        InterfaceExtendsClauseContext stsInterfaceExtendsClause = stsInterfaceDeclaration.interfaceExtendsClause();
        if (stsInterfaceExtendsClause != null) {
            sb.append(' ');
            visitInterfaceExtendsClause(stsInterfaceExtendsClause);
        }

        sb.append(" {\n");

        indentIncrement();
        visitInterfaceBody(stsInterfaceDeclaration.interfaceBody());
        indentDecrement();

        sb.append(indentCurrent).append("}\n\n");

        return null;
    }

    // interfaceBody: interfaceMember+
    @Override
    public Void visitInterfaceBody(InterfaceBodyContext stsInterfaceBody) {
        for (InterfaceMemberContext stsInterfaceMember : stsInterfaceBody.interfaceMember()) {
            doNeededIndent();
            stsInterfaceMember.accept(this);
        }

        return null;
    }

    // : Identifier signature SemiColon                                           #InterfaceMethod
    @Override
    public Void visitInterfaceMethod(InterfaceMethodContext stsInterfaceMethod) {
        sb.append(stsInterfaceMethod.Identifier().getText());
        visitSignature(stsInterfaceMethod.signature());
        sb.append(";\n");
        return null;
    }

    // | (Static | Private)? methodSignature block   #InterfaceMethodWithBody
    @Override
    public Void visitInterfaceMethodWithBody(InterfaceMethodWithBodyContext stsInterfaceMethodWithBody) {
        modifierWriteSafe(stsInterfaceMethodWithBody.Private());
        modifierWriteSafe(stsInterfaceMethodWithBody.Static());

        sb.append(stsInterfaceMethodWithBody.Identifier().getText());
        visitSignature(stsInterfaceMethodWithBody.signature());

        visitBlock(stsInterfaceMethodWithBody.block());

        return null;
    }

    // | constantDeclaration SemiColon       #InterfaceField
    @Override
    public Void visitInterfaceField(InterfaceFieldContext stsInterfaceField) {
        doNeededIndent();
        visitConstantDeclaration(stsInterfaceField.constantDeclaration());
        sb.append(";\n");

        return null;
    }

    // | interfaceDeclaration                                                #InterfaceInInterface
    @Override
    public Void visitInterfaceInInterface(InterfaceInInterfaceContext stsInnerInterface) {
        visitInterfaceDeclaration(stsInnerInterface.interfaceDeclaration());
        return null;
    }

    // | classDeclaration                                                    #ClassInInterface
    @Override
    public Void visitClassInInterface(ClassInInterfaceContext stsInnerClass) {
        visitClassDeclaration(stsInnerClass.classDeclaration());
        return null;
    }

    @Override
    public Void visitEnumInInterface(EnumInInterfaceContext stsInnerEnum) {
        visitEnumDeclaration(stsInnerEnum.enumDeclaration());
        return null;
    }

    // interfaceExtendsClause: Extends classOrInterfaceTypeList
    @Override
    public Void visitInterfaceExtendsClause(InterfaceExtendsClauseContext stsInterfaceExtendsClause) {
        sb.append(stsInterfaceExtendsClause.Extends().getText()).append(' ');
        visitInterfaceTypeList(stsInterfaceExtendsClause.interfaceTypeList());
        return null;
    }

    // classOrInterfaceTypeList: typeReference (',' typeReference)*
    @Override
    public Void visitInterfaceTypeList(InterfaceTypeListContext stsClassOrInterfaceTypeList) {
        int i = 0;
        for (TypeReferenceContext stsTypeReference : stsClassOrInterfaceTypeList.typeReference()) {
            if (i > 0) sb.append(", ");
            visitTypeReference(stsTypeReference);
            ++i;
        }

        return null;
    }

    // enumDeclaration: Enum Identifier '{' enumBody? '}'
    @Override
    public Void visitEnumDeclaration(EnumDeclarationContext stsEnumDeclaration) {
        doNeededIndent();
        sb.append(stsEnumDeclaration.Enum().getText()).append(' ');
        sb.append(stsEnumDeclaration.Identifier().getText());

        sb.append(" {\n");

        EnumBodyContext stsEnumBody = stsEnumDeclaration.enumBody();
        if (stsEnumBody != null) {
            indentIncrement();
            visitEnumBody(stsEnumBody);
            indentDecrement();

            sb.append('\n');
        }

        sb.append(indentCurrent).append("}\n");

        return null;
    }

    // enumBody: enumMember (',' enumMember)*
    @Override
    public Void visitEnumBody(EnumBodyContext stsEnumBody) {
        boolean isFirstMember = true;
        for (EnumMemberContext stsEnumMember : stsEnumBody.enumMember()) {
            if (!isFirstMember) {
                sb.append(",\n");
            } else {
                isFirstMember = false;
            }

            doNeededIndent();
            visitEnumMember(stsEnumMember);
        }

        return null;
    }

    // enumMember: Identifier ('=' singleExpression)?
    @Override
    public Void visitEnumMember(EnumMemberContext stsEnumMember) {
        sb.append(stsEnumMember.Identifier().getText());

        SingleExpressionContext stsInitializer = stsEnumMember.singleExpression();
        if (stsInitializer != null) {
            sb.append(" = ");
            stsInitializer.accept(this);
        }

        return null;
    }

    // compilationUnit: packageDeclaration? importStatement* topDeclaration* EOF;
    @Override
    public Void visitCompilationUnit(CompilationUnitContext stsCompilationUnit) {
        PackageDeclarationContext stsPackageDecl = stsCompilationUnit.packageDeclaration();
        if (stsPackageDecl != null) visitPackageDeclaration(stsPackageDecl);

        for (ImportDeclarationContext stsImportDeclaration : stsCompilationUnit.importDeclaration()) {
            visitImportDeclaration(stsImportDeclaration);
        }

        for (TopDeclarationContext stsTopDeclaration : stsCompilationUnit.topDeclaration()) {
            stsTopDeclaration.accept(this);
        }

        return null;
    }

    @Override
    public Void visitTopDeclaration(TopDeclarationContext stsTopDeclaration) {
        int declIndex = 0;
        TerminalNode termExport = stsTopDeclaration.Export();
        if (termExport != null) {
            sb.append(termExport.getText()).append(' ');
            declIndex = 1;
        }
        assert(stsTopDeclaration.getChildCount() == declIndex+1);
        stsTopDeclaration.getChild(declIndex).accept(this);

        return null;
    }

    @Override
    public Void visitPackageDeclaration(PackageDeclarationContext stsPackageDecl) {
        sb.append(stsPackageDecl.Package().getText()).append(' ');
        visitQualifiedName(stsPackageDecl.qualifiedName());
        sb.append(";\n\n");

        return null;
    }

    //    statement
    //    : block
    //    | assertStatement
    //    | ifStatement
    //    | iterationStatement
    //    | continueStatement
    //    | breakStatement
    //    | returnStatement
    //    | labelledStatement
    //    | switchStatement
    //    | throwStatement
    //    | deferStatement
    //    | trapStatement
    //    | expressionStatement
    @Override
    public Void visitStatement(StatementContext stsStatement) {
        doNeededIndent();

        assert(stsStatement.getChildCount() == 1);
        stsStatement.getChild(0).accept(this);
        return null;
    }

    // block: '{' statementOrLocalDeclaration* '}'
    @Override
    public Void visitBlock(BlockContext stsBlock) {
        doNeededIndent();
        sb.append("{\n");
        indentIncrement();

        List<StatementOrLocalDeclarationContext> stsStatementList = stsBlock.statementOrLocalDeclaration();
        if (stsStatementList != null) {
            for (StatementOrLocalDeclarationContext stsStatementOrLocalDecl : stsStatementList)
                visitStatementOrLocalDeclaration(stsStatementOrLocalDecl);
        }
        indentDecrement();
        sb.append(indentCurrent).append("}\n");

        return null;
    }

    // importDeclaration: Import qualifiedName (Dot Multiply)? SemiColon?
    public Void visitImportDeclaration(ImportDeclarationContext stsImportDeclatation) {
        doNeededIndent();
        sb.append(stsImportDeclatation.Import().getText()).append(' ');

        visitQualifiedName(stsImportDeclatation.qualifiedName());

        TerminalNode termDot = stsImportDeclatation.Dot();
        TerminalNode termAs = stsImportDeclatation.As();
        if (termDot != null) {
            sb.append(termDot.getText());
            TerminalNode termMult = stsImportDeclatation.Multiply();
            assert(termMult != null);
            sb.append(termMult.getText());
        }
        else if (termAs != null) {
            sb.append(termAs.getText()).append(' ');
            TerminalNode termId = stsImportDeclatation.Identifier();
            assert(termId != null);
            sb.append(termId.getText());
        }

        sb.append(";\n");
        return null;
    }

    @Override
    public Void visitAssertStatement(AssertStatementContext stsAssertStatement) {
        TerminalNode termAssert = stsAssertStatement.Assert();
        sb.append(termAssert.getText()).append(' ');

        assert(stsAssertStatement.condition != null);
        stsAssertStatement.condition.accept(this);

        if (stsAssertStatement.message != null) {
            sb.append(" : ");
            stsAssertStatement.message.accept(this);
        }

        sb.append(";\n");
        return null;
    }

    // variableOrConstDeclaration: ((Let variableDeclarationList) | (Const constantDeclarationList)) SemiColon
    @Override
    public Void visitVariableOrConstantDeclaration(VariableOrConstantDeclarationContext stsVarOrConstDeclaration) {
        doNeededIndent();

        TerminalNode termLet = stsVarOrConstDeclaration.Let();
        if (termLet != null) {
            sb.append(termLet.getText()).append(' ');
            visitVariableDeclarationList(stsVarOrConstDeclaration.variableDeclarationList());
        }
        else {
            assert (stsVarOrConstDeclaration.Const() != null);
            modifierWriteSafe(stsVarOrConstDeclaration.Const());
            visitConstantDeclarationList(stsVarOrConstDeclaration.constantDeclarationList());
        }

        sb.append(";\n");

        return null;
    }

    // variableDeclarationList: variableDeclaration (',' variableDeclaration)*
    @Override
    public Void visitVariableDeclarationList(VariableDeclarationListContext stsVariableDeclarationList) {
        int i = 0;
        for (VariableDeclarationContext stsVariableDeclaration : stsVariableDeclarationList.variableDeclaration()) {
            if (i > 0) sb.append(", ");
            visitVariableDeclaration(stsVariableDeclaration);
            ++i;
        }
        return null;
    }

    // constantDeclarationList: constantDeclaration (',' constantDeclaration)*
    @Override
    public Void visitConstantDeclarationList(ConstantDeclarationListContext stsConstantDeclarationList) {
        int i = 0;
        for (ConstantDeclarationContext stsConstantDeclaration : stsConstantDeclarationList.constantDeclaration()) {
            if (i > 0) sb.append(", ");
            visitConstantDeclaration(stsConstantDeclaration);
            ++i;
        }

        return null;
    }

    // variableDeclaration    : Identifier typeAnnotation initializer? | Identifier initializer
    @Override
    public Void visitVariableDeclaration(VariableDeclarationContext stsVariableDeclaration) {
        sb.append(stsVariableDeclaration.Identifier().getText()).append(' ');

        TypeAnnotationContext stsTypeAnnotation = stsVariableDeclaration.typeAnnotation();
        if (stsTypeAnnotation != null) {
            visitTypeAnnotation(stsTypeAnnotation);
        }

        InitializerContext stsInitializer = stsVariableDeclaration.initializer();
        if (stsInitializer != null) {
            visitInitializer(stsInitializer);
        }

        return null;
    }

    // constantDeclaration: Identifier typeAnnotation? initializer
    @Override
    public Void visitConstantDeclaration(ConstantDeclarationContext stsConstantDeclaration) {
        TerminalNode termIdentifier = stsConstantDeclaration.Identifier();
        sb.append(termIdentifier.getText()).append(' ');

        TypeAnnotationContext stsTypeAnnotation = stsConstantDeclaration.typeAnnotation();
        if (stsTypeAnnotation != null) {
            visitTypeAnnotation(stsTypeAnnotation);
        }

        InitializerContext stsInitializer = stsConstantDeclaration.initializer();
        if (stsInitializer != null) {
            visitInitializer(stsInitializer);
        }

        return null;
    }

    // expressionStatement: singleExpression SemiColon?
    @Override
    public Void visitExpressionStatement(ExpressionStatementContext stsExpressionStatement) {
        doNeededIndent();
        stsExpressionStatement.singleExpression().accept(this);
        sb.append(";\n");

        return null;
    }

    // ifStatement: If OpenParent singleExpression CloseParen ifStmt=statement (Else elseStmt=statement)?
    @Override
    public Void visitIfStatement(IfStatementContext stsIfStatement) {
        doNeededIndent();
        sb.append(stsIfStatement.If().getText()).append(" (");

        stsIfStatement.singleExpression().accept(this);

        sb.append(") ");

        assert(stsIfStatement.ifStmt != null);
        visitStatement(stsIfStatement.ifStmt);

        TerminalNode termElse = stsIfStatement.Else();
        if (termElse != null) {
            sb.append(indentCurrent).append(termElse.getText()).append(" ");

            assert(stsIfStatement.elseStmt != null);
            visitStatement(stsIfStatement.elseStmt);
        }

        return null;
    }

    // : Do statement* While '(' singleExpression ')' SemiColon # DoStatement
    @Override
    public Void visitDoStatement(DoStatementContext stsDoStatement) {
        doNeededIndent();
        sb.append(stsDoStatement.Do().getText()).append("\n");

        StatementContext stsStmt = stsDoStatement.statement();
        assert(stsStmt != null);
        visitStatement(stsStmt);

        sb.append(indentCurrent).append(stsDoStatement.While().getText()).append('(');

        stsDoStatement.singleExpression().accept(this);

        sb.append(");\n");

        return null;
    }

    // | While '(' singleExpression ')' (statement | block) # WhileStatement
    @Override
    public Void visitWhileStatement(WhileStatementContext stsWhileStatement) {
        doNeededIndent();
        sb.append(stsWhileStatement.While().getText()).append('(');
        stsWhileStatement.singleExpression().accept(this);
        sb.append(")\n");

        StatementContext stsStmt = stsWhileStatement.statement();
        assert (stsStmt != null);
        visitStatement(stsStmt);

        return null;
    }

    // | For '(' forInit? SemiColon singleExpression? SemiColon expressionSequence ')' statement  # ForStatement
    @Override
    public Void visitForStatement(ForStatementContext stsForStatement) {
        doNeededIndent();
        sb.append(stsForStatement.For().getText()).append(" (");

        ForInitContext stsForInit = stsForStatement.forInit();
        if (stsForInit != null) {
            visitForInit(stsForInit);
        }

        sb.append("; ");

        SingleExpressionContext stsCondition = stsForStatement.singleExpression();
        if (stsCondition != null) {
            stsCondition.accept(this);
        }

        sb.append("; ");

        ExpressionSequenceContext stsUpdaters = stsForStatement.expressionSequence();
        if (stsUpdaters != null) {
            visitExpressionSequence(stsUpdaters);
        }

        sb.append(") ");

        StatementContext stsStmt = stsForStatement.statement();
        assert(stsStmt != null);
        visitStatement(stsStmt);

        return null;
    }

    // forInit: ExpressionSequence | Let variableDeclarationList
    @Override
    public Void visitForInit(ForInitContext stsForInit) {
        ExpressionSequenceContext stsExprSeq = stsForInit.expressionSequence();
        if (stsExprSeq != null) {
            visitExpressionSequence(stsExprSeq);
        } else {
            sb.append(stsForInit.Let().getText()).append(' ');
            visitVariableDeclarationList(stsForInit.variableDeclarationList());
        }

        return null;
    }

    // expressionSequence: singleExpression (',' singleExpression)*
    @Override
    public Void visitExpressionSequence(ExpressionSequenceContext stsExpressionSequence) {
        int i = 0;
        for (SingleExpressionContext stsExpression : stsExpressionSequence.singleExpression()) {
            if (i > 0) sb.append(", ");
            stsExpression.accept(this);
            ++i;
        }

       return null;
    }

     // | For '(' Let Identifier typeAnnotation? Of singleExpression ')' (statement | block)  # ForInOfStatement
    @Override
    public Void visitForOfStatement(ForOfStatementContext stsForOfStatement) {
        doNeededIndent();
        sb.append(stsForOfStatement.For().getText()).append(" (");
        sb.append(stsForOfStatement.Let().getText()).append(' ');
        sb.append(stsForOfStatement.Identifier().getText()).append(' ');

        TypeAnnotationContext stsTypeAnnotation = stsForOfStatement.typeAnnotation();
        if (stsTypeAnnotation != null) {
            visitTypeAnnotation(stsTypeAnnotation);
        }

        sb.append(stsForOfStatement.Of().getText());

        sb.append(' ');
        stsForOfStatement.singleExpression().accept(this);
        sb.append(')');

        StatementContext stsStmt = stsForOfStatement.statement();
        assert(stsStmt != null);
        visitStatement(stsStmt);

        return null;
    }

    // continueStatement: Continue Identifier? SemiColon
    @Override
    public Void visitContinueStatement(ContinueStatementContext stsContinueStatement) {
        doNeededIndent();
        sb.append(stsContinueStatement.Continue().getText());

        TerminalNode termIdentifier = stsContinueStatement.Identifier();
        if (termIdentifier != null) {
            sb.append(' ').append(termIdentifier.getText());
        }

        sb.append(";\n");

        return null;
    }

    // breakStatement: Break Identifier? SemiColon
    @Override
    public Void visitBreakStatement(BreakStatementContext stsBreakStatement) {
        doNeededIndent();
        sb.append(stsBreakStatement.Break().getText());

        TerminalNode termIdentifier = stsBreakStatement.Identifier();
        if (termIdentifier != null) {
            sb.append(' ').append(termIdentifier.getText());
        }

        sb.append(";\n");

        return null;
    }

    // returnStatement: Return (singleExpression)? SemiColon
    @Override
    public Void visitReturnStatement(ReturnStatementContext stsReturnStatement) {
        doNeededIndent();
        sb.append(stsReturnStatement.Return().getText());

        SingleExpressionContext stsSingleExpression = stsReturnStatement.singleExpression();
        if (stsSingleExpression != null) {
            sb.append(' ');
            stsSingleExpression.accept(this);
        }

        sb.append(";\n");

        return null;
    }

    //    | (typeReference Dot)? This  # ThisExpression
    @Override
    public Void visitThisExpression(ThisExpressionContext stsThisExpression) {
        TypeReferenceContext stsTypeReference = stsThisExpression.typeReference();
        if (stsTypeReference != null) {
            visitTypeReference(stsTypeReference);
            sb.append('.');
        }

        sb.append(stsThisExpression.This().getText());
        return null;
    }

    //    | Identifier                                                             # IdentifierExpression
    @Override
    public Void visitIdentifierExpression(IdentifierExpressionContext stsIdentifierExpression) {
        sb.append(stsIdentifierExpression.Identifier().getText());
        return null;
    }

    //    | (typereference Dot)? Super # SuperExpression
    @Override
    public Void visitSuperExpression(SuperExpressionContext stsSuperExpression) {
        TypeReferenceContext stsTypeReference = stsSuperExpression.typeReference();
        if (stsTypeReference != null) {
            visitTypeReference(stsTypeReference);
            sb.append('.');
        }

        sb.append(stsSuperExpression.Super().getText());
        return null;
    }

        // switchStatement: Switch '(' singleExpression ')' caseBlock
    @Override
    public Void visitSwitchStatement(SwitchStatementContext stsSwitchStatement) {
        doNeededIndent();

        sb.append(stsSwitchStatement.Switch().getText()).append(" (");
        stsSwitchStatement.singleExpression().accept(this);
        sb.append(')');

        visitCaseBlock(stsSwitchStatement.caseBlock());
        sb.append('\n');

        return null;
    }

    // caseBlock: '{' leftCases=caseClauses? defaultClause? rightCases=caseClauses? '}'
    @Override
    public Void visitCaseBlock(CaseBlockContext stsCaseBlock) {
        if (!doNeededIndent()) {
            sb.append(' ');
        }

        sb.append("{\n");
        indentIncrement();

        if (stsCaseBlock.leftCases != null) {
            visitCaseClauses(stsCaseBlock.leftCases);
        }

        DefaultClauseContext stsDefaultClause = stsCaseBlock.defaultClause();
        if (stsDefaultClause != null) {
            visitDefaultClause(stsDefaultClause);
        }

        if (stsCaseBlock.rightCases != null) {
            visitCaseClauses(stsCaseBlock.rightCases);
        }

        indentDecrement();
        sb.append(indentCurrent).append("}\n");

        return null;
    }

    // caseClauses: caseClause+
    @Override
    public Void visitCaseClauses(CaseClausesContext stsCaseClauses) {
        List<CaseClauseContext> stsCaseClauseList = stsCaseClauses.caseClause();
        for (CaseClauseContext stsCaseClause : stsCaseClauseList) {
            visitCaseClause(stsCaseClause);
        }

        return null;
    }

    // caseClause: Case singleExpression ':' statement*
    @Override
    public Void visitCaseClause(CaseClauseContext stsCaseClause) {
        doNeededIndent();
        sb.append(stsCaseClause.Case().getText()).append(' ');
        stsCaseClause.singleExpression().accept(this);
        sb.append(":\n");

        List<StatementContext> stsStatementList = stsCaseClause.statement();

        indentIncrement();
        for (StatementContext stsStatement : stsStatementList)
            visitStatement(stsStatement);
        indentDecrement();

        return null;
    }

    // defaultClause: Default ':' statement*
    @Override
    public Void visitDefaultClause(DefaultClauseContext stsDefaultClause) {
        doNeededIndent();
        sb.append(stsDefaultClause.Default().getText()).append(":\n");

        List<StatementContext> stsStatementList = stsDefaultClause.statement();

        indentIncrement();
        for (StatementContext stsStatement : stsStatementList)
            visitStatement(stsStatement);
        indentDecrement();

        return null;
    }

    // labelledStatement: Identifier ':' statement
    @Override
    public Void visitLabelledStatement(LabelledStatementContext stsLabelledStatement) {
        doNeededIndent();
        sb.append(stsLabelledStatement.Identifier().getText()).append(": ");
        // after removing Block node from statements we got hanging labels before java blocks
        if( stsLabelledStatement.statement() !=null )
            visitStatement(stsLabelledStatement.statement());


        return null;
    }

    // throwStatement: Throw singleExpression SemiColon
    @Override
    public Void visitThrowStatement(ThrowStatementContext stsThrowStatement) {
        doNeededIndent();
        sb.append(stsThrowStatement.Throw().getText()).append(' ');
        stsThrowStatement.singleExpression().accept(this);
        sb.append(";\n");

        return null;
    }

    // trapStatement: Trap block catchOrRecoverClause+;
    @Override
    public Void visitTrapStatement(TrapStatementContext stsTrapStatement) {
        doNeededIndent();
        sb.append(stsTrapStatement.Trap().getText()).append(' ');

        visitBlock(stsTrapStatement.block());

        List<CatchOrRecoverClauseContext> stsCatchesOrRecovers = stsTrapStatement.catchOrRecoverClause();
        assert(stsCatchesOrRecovers != null && !stsCatchesOrRecovers.isEmpty());
        for (CatchOrRecoverClauseContext stsCatchOrRecover : stsCatchesOrRecovers) {
            visitCatchOrRecoverClause(stsCatchOrRecover);
        }

        sb.append('\n');
        return null;
    }

    // catchOrRecoverClause: (Catch|Recover) exceptionParameter? block

    @Override
    public Void visitCatchOrRecoverClause(CatchOrRecoverClauseContext stsCatchOrRecoverClause) {
        doNeededIndent();

        TerminalNode stsTerm = stsCatchOrRecoverClause.Catch();
        if (stsTerm == null) stsTerm = stsCatchOrRecoverClause.Recover();
        assert(stsTerm != null);
        sb.append(stsTerm.getText()).append(' ');

        ExceptionParameterContext stsExceptionParam = stsCatchOrRecoverClause.exceptionParameter();
        if (stsExceptionParam != null) {
            visitExceptionParameter(stsExceptionParam);
        }

        BlockContext stsBlock = stsCatchOrRecoverClause.block();
        assert(stsBlock != null);
        visitBlock(stsBlock);

        return null;
    }

    @Override
    public Void visitExceptionParameter(ExceptionParameterContext stsExceptionParam) {
        sb.append('(').append(stsExceptionParam.Identifier().getText()).append(' ');
        visitTypeAnnotation(stsExceptionParam.typeAnnotation());
        sb.append(") ");

        return null;
    }

    // functionDeclaration: Function Identifier signature block
    @Override
    public Void visitFunctionDeclaration(FunctionDeclarationContext stsFunctionDeclaration) {
        doNeededIndent();
        sb.append(stsFunctionDeclaration.Function().getText()).append(' ');
        sb.append(stsFunctionDeclaration.Identifier().getText());

        visitSignature(stsFunctionDeclaration.signature());

        visitBlock(stsFunctionDeclaration.block());

        return null;
    }

    private void modifierWriteSafe(TerminalNode term) {
        if (term != null)
            sb.append(term.getText()).append(' ');
    }

    // classDeclaration:
    //   (Static? (Abstract | Open) | (Abstract | Open) Static)?
    //      Class Identifier typeParameters? classExtendsClause? implementsClause? classBody
    @Override
    public Void visitClassDeclaration(ClassDeclarationContext stsClassDeclaration) {
        doNeededIndent();

        modifierWriteSafe(stsClassDeclaration.Abstract());
        modifierWriteSafe(stsClassDeclaration.Static());
        modifierWriteSafe(stsClassDeclaration.Open());

        sb.append(stsClassDeclaration.Class().getText()).append(' ');
        sb.append(stsClassDeclaration.Identifier().getText());

        TypeParametersContext stsTypeParameters = stsClassDeclaration.typeParameters();
        if (stsTypeParameters != null) {
            visitTypeParameters(stsTypeParameters);
        }
        else {
            sb.append(' ');
        }

        ClassExtendsClauseContext stsClassExtends = stsClassDeclaration.classExtendsClause();
        if (stsClassExtends != null) {
            visitClassExtendsClause(stsClassExtends);
        }

        ImplementsClauseContext stsImplements = stsClassDeclaration.implementsClause();
        if (stsImplements != null) {
            visitImplementsClause(stsImplements);
        }

        visitClassBody(stsClassDeclaration.classBody());

        return null;
    }

    // classBody:  '{' classElement* '}'
    @Override
    public Void visitClassBody(ClassBodyContext stsClassBody) {
        sb.append(" {\n");
        indentIncrement();

        visitChildren(stsClassBody);

        indentDecrement();
        sb.append(indentCurrent).append("}");

        // Don't start new line if this class body is a part of a object creation expression
        if (!(stsClassBody.getParent() instanceof NewClassExpressionContext))
            sb.append("\n\n");

        return null;
    }

    // classInitializer
    @Override
    public Void visitClassInitializer(ClassInitializerContext stsClassInit) {
        doNeededIndent();

        modifierWriteSafe(stsClassInit.Static());

        BlockContext stsBlock = stsClassInit.block();
        assert(stsBlock != null);
        visitBlock(stsBlock);

        return null;
    }

    // classFieldDeclaration
    //    : Static? (variableDeclaration | Const constantDeclaration) SemiColon
    //    | Const Static? constantDeclaration SemiColon
    @Override
    public Void visitClassFieldDeclaration(ClassFieldDeclarationContext stsClassField) {
        doNeededIndent();

        modifierWriteSafe(stsClassField.Static());

        VariableDeclarationContext stsVarDecl = stsClassField.variableDeclaration();
        if (stsVarDecl != null) {
            visitVariableDeclaration(stsVarDecl);
        }
        else {
            assert(stsClassField.Const() != null);
            modifierWriteSafe(stsClassField.Const());

            ConstantDeclarationContext stsConstDecl = stsClassField.constantDeclaration();
            assert(stsConstDecl != null);
            visitConstantDeclaration(stsConstDecl);
        }

        sb.append(";\n");

        return null;
    }

    // : (Static | Override | Open)? Identifier signature block    #ClassMethodWithBody
    @Override
    public Void visitClassMethodWithBody(ClassMethodWithBodyContext stsClassMethodWithBody) {
        doNeededIndent();

        modifierWriteSafe(stsClassMethodWithBody.Static());
        modifierWriteSafe(stsClassMethodWithBody.Override());
        modifierWriteSafe(stsClassMethodWithBody.Open());

        sb.append(stsClassMethodWithBody.Identifier().getText());
        visitSignature(stsClassMethodWithBody.signature());

        visitBlock(stsClassMethodWithBody.block());

        return null;
    }

    // | (Abstract | Static? Native | Native Static) Identifier signature SemiColon                                   #AbstractClassMethod
    @Override
    public Void visitAbstractOrNativeClassMethod(AbstractOrNativeClassMethodContext stsAbstractMethod) {
        doNeededIndent();

        modifierWriteSafe(stsAbstractMethod.Abstract());
        modifierWriteSafe(stsAbstractMethod.Static());
        modifierWriteSafe(stsAbstractMethod.Native());

        sb.append(stsAbstractMethod.Identifier().getText());
        visitSignature(stsAbstractMethod.signature());

        sb.append(";\n");

        return null;
    }

    // classExtendsClause: Extends typeReference
    @Override
    public Void visitClassExtendsClause(ClassExtendsClauseContext stsClassExtendsClause) {
        sb.append(stsClassExtendsClause.Extends().getText()).append(' ');
        visitTypeReference(stsClassExtendsClause.typeReference());
        sb.append(' ');
        return null;
    }

    // implementsClause: Implements classOrInterfaceTypeList
    @Override
    public Void visitImplementsClause(ImplementsClauseContext stsImplementsClause) {
        sb.append(stsImplementsClause.Implements().getText()).append(' ');
        visitInterfaceTypeList(stsImplementsClause.interfaceTypeList());
        sb.append(' ');
        return null;
    }

    // parameterList
    //    : parameter (',' parameter)* (',' variadicParameter)?
    //    | variadicParameter
    @Override
    public Void visitParameterList(ParameterListContext stsParameterList) {
        List<ParameterContext> stsParameters = stsParameterList.parameter();

        if (stsParameters != null) {
            int i = 0;
            for (ParameterContext stsParameter : stsParameters) {
                if (i > 0) sb.append(", ");
                visitParameter(stsParameter);
                ++i;
            }
        }

        VariadicParameterContext stsVariadicParameter = stsParameterList.variadicParameter();
        if (stsVariadicParameter != null) {
            sb.append(", ");
            visitVariadicParameter(stsVariadicParameter);
        }

        return null;
    }

    // parameter: Identifier typeAnnotation
    @Override
    public Void visitParameter(ParameterContext stsParameter) {
        TerminalNode termIdentifier = stsParameter.Identifier();
        if (termIdentifier != null) {
            sb.append(termIdentifier.getText()).append(' ');
        }

        visitTypeAnnotation(stsParameter.typeAnnotation());

        return null;
    }

    // variadicParameter: Ellipsis Identifier typeAnnotation
    @Override
    public Void visitVariadicParameter(VariadicParameterContext stsVariadicParameter) {
        sb.append(stsVariadicParameter.Ellipsis().getText()).append(' ');
        sb.append(stsVariadicParameter.Identifier().getText());
        visitTypeAnnotation(stsVariadicParameter.typeAnnotation());
        return null;
    }

    // | OpenBracked expressionSequence? CloseBracket  #ArrayLiterlaExpression
    @Override
    public Void visitArrayLiteralExpression(ArrayLiteralExpressionContext stsArrayLiteral) {
        sb.append('[');

        ExpressionSequenceContext stsExpressions = stsArrayLiteral.expressionSequence();
        if (stsExpressions != null) visitExpressionSequence(stsExpressions);

        sb.append(']');

        return null;
    }

    //     | primaryType '.' Class                                                  # ClassLiteralExpression
    @Override
    public Void visitClassLiteralExpression(ClassLiteralExpressionContext stsClassLiteral) {
        stsClassLiteral.primaryType().accept(this);
        sb.append(stsClassLiteral.Dot().getText()).append(stsClassLiteral.Class().getText());
        return null;
    }

    //    | '(' singleExpression ')'                                             # ParenthesizedExpression
    @Override
    public Void visitParenthesizedExpression(ParenthesizedExpressionContext stsParenthesizedExpression) {
        sb.append('(');
        stsParenthesizedExpression.singleExpression().accept(this);
        sb.append(')');
        return null;
    }

    //    | singleExpression As asExpression                                       # CastAsExpression;
    @Override
    public Void visitCastExpression(CastExpressionContext stsCastExpression) {
        stsCastExpression.singleExpression().accept(this);

        sb.append(' ').append(stsCastExpression.As().getText()).append(' ');

        IntersectionTypeContext stsIntersectionType = stsCastExpression.intersectionType();
        if (stsIntersectionType != null) {
            visitIntersectionType(stsIntersectionType);
        }
        else {
            PrimaryTypeContext stsPrimaryType = stsCastExpression.primaryType();
            assert stsPrimaryType != null;
            stsPrimaryType.accept(this);
        }

        return null;
    }

    // arguments: '(' argumentList? ')'
    @Override
    public Void visitArguments(ArgumentsContext stsArguments) {
        sb.append('(');

        ExpressionSequenceContext stsArgumentList = stsArguments.expressionSequence();
        if (stsArgumentList != null)
            visitExpressionSequence(stsArgumentList);
        else if (stsArguments.getChildCount() > 0)
            visitChildren(stsArguments);

        sb.append(')');

        return null;
    }

    @Override
    public Void visitTerminal(TerminalNode stsTerminal) {
        int stsTerminalCode = stsTerminal.getSymbol().getType();
        if (stsTerminalCode == StaticTSLexer.MultiLineComment ||
            stsTerminalCode == StaticTSLexer.SingleLineComment) {
            sb.append(stsTerminal.getText());
        }

        return null;
    }

    //    | singleExpression Instanceof singleExpression                           # InstanceofExpression
    @Override
    public Void visitInstanceofExpression(InstanceofExpressionContext stsInstanceofExpression) {
        stsInstanceofExpression.singleExpression().accept(this);
        sb.append(' ').append(stsInstanceofExpression.Instanceof().getText()).append(' ');
        stsInstanceofExpression.primaryType().accept(this);

        return null;
    }

    private void preOperatorExpressionWrite(String op, SingleExpressionContext stsSingleExpression) {
        sb.append(op);
        stsSingleExpression.accept(this);
    }

    private void postOperatorExpressionWrite(String op, SingleExpressionContext stsSingleExpression) {
        stsSingleExpression.accept(this);
        sb.append(op);
    }

    private void binaryOperatorWrite(String op, List<SingleExpressionContext> operands) {
        operands.get(0).accept(this);
        sb.append(' ').append(op).append(' ');
        operands.get(1).accept(this);
    }

    // | singleExpression typeArguments? arguments #CallExpression
    @Override
    public Void visitCallExpression(CallExpressionContext stsCallExpression) {
        stsCallExpression.singleExpression().accept(this);

        TypeArgumentsContext stsTypeArguments = stsCallExpression.typeArguments();
        if (stsTypeArguments != null) {
            visitTypeArguments(stsTypeArguments);
        }

        visitArguments(stsCallExpression.arguments());

        return null;
    }

    // | New typeArguments? (singleExpression Dot)? typeReference arguments? classBody? # NewClassExpression
    @Override
    public Void visitNewClassExpression(NewClassExpressionContext stsNewClassExpression) {
        sb.append(stsNewClassExpression.New().getText()).append(' ');

        TypeArgumentsContext stsTypeArguments = stsNewClassExpression.typeArguments();
        if (stsTypeArguments != null) visitTypeArguments(stsTypeArguments);

        SingleExpressionContext stsOuterObject = stsNewClassExpression.singleExpression();
        if (stsOuterObject != null) {
            stsOuterObject.accept(this);
            sb.append('.');
        }

        visitTypeReference(stsNewClassExpression.typeReference());

        ArgumentsContext stsArguments = stsNewClassExpression.arguments();
        if (stsArguments != null) {
            visitArguments(stsArguments);
        }

        ClassBodyContext stsClassBody = stsNewClassExpression.classBody();
        if (stsClassBody != null) {
            visitClassBody(stsClassBody);
        }

        return null;
    }

    // | New primaryType indexExpression+ (OpenBracket CloseBracket)* # NewArrayExpression
    @Override
    public Void visitNewArrayExpression(NewArrayExpressionContext stsNewArrayExpression) {
        TerminalNode stsTerm = stsNewArrayExpression.New();
        sb.append(stsTerm.getText()).append(' ');

        PrimaryTypeContext stsPrimaryType = stsNewArrayExpression.primaryType();
        visitPrimaryType(stsPrimaryType);

        List<IndexExpressionContext> stsIndexList = stsNewArrayExpression.indexExpression();
        assert(stsIndexList != null && !stsIndexList.isEmpty());
        for (IndexExpressionContext stsIndex : stsIndexList)
            visitIndexExpression(stsIndex);

        List<TerminalNode> emptyDims = stsNewArrayExpression.OpenBracket();
        if (emptyDims != null && !emptyDims.isEmpty()) {
            assert(stsNewArrayExpression.CloseBracket().size() == emptyDims.size());
            for (int i = 0; i < emptyDims.size(); ++i) {
                sb.append("[]");
            }
        }

        return null;
    }

    @Override
    public Void visitIndexExpression(IndexExpressionContext stsIndexExpression) {
        sb.append('[');

        SingleExpressionContext stsExpression = stsIndexExpression.singleExpression();
        assert(stsExpression != null);
        stsExpression.accept(this);

        sb.append(']');

        return null;
    }

    //  | singleExpression Dot Identifier   # MemberDotExpression
    @Override
    public Void visitMemberAccessExpression(MemberAccessExpressionContext stsMemberAccessExpression) {
        stsMemberAccessExpression.singleExpression().accept(this);
        sb.append('.').append(stsMemberAccessExpression.Identifier().getText());
        return null;
    }

    //    | singleExpression {this.notLineTerminator()}? '--'                      # PostDecreaseExpression
    @Override
    public Void visitPostDecreaseExpression(PostDecreaseExpressionContext stsPostDecreaseExpression) {
        postOperatorExpressionWrite("--", stsPostDecreaseExpression.singleExpression());
        return null;
    }

    //    | singleExpression {this.notLineTerminator()}? '++'                      # PostIncrementExpression
    @Override
    public Void visitPostIncrementExpression(PostIncrementExpressionContext stsPostIncrementExpression) {
        postOperatorExpressionWrite("++", stsPostIncrementExpression.singleExpression());
        return null;
    }

    //    | '++' singleExpression                                                  # PreIncrementExpression
    @Override
    public Void visitPreIncrementExpression(PreIncrementExpressionContext stsPreIncrementExpression) {
        preOperatorExpressionWrite("++", stsPreIncrementExpression.singleExpression());
        return null;
    }

    //    | '--' singleExpression                                                  # PreDecreaseExpression
    @Override
    public Void visitPreDecreaseExpression(PreDecreaseExpressionContext stsPreDecreaseExpression) {
        preOperatorExpressionWrite("--", stsPreDecreaseExpression.singleExpression());
        return null;
    }

    //    | '+' singleExpression                                                   # UnaryPlusExpression
    @Override
    public Void visitUnaryPlusExpression(UnaryPlusExpressionContext stsUnaryPlusExpression) {
        preOperatorExpressionWrite("+", stsUnaryPlusExpression.singleExpression());
        return null;
    }

    //    | '-' singleExpression                                                   # UnaryMinusExpression
    @Override
    public Void visitUnaryMinusExpression(UnaryMinusExpressionContext stsUnaryMinusExpression) {
        preOperatorExpressionWrite("-", stsUnaryMinusExpression.singleExpression());
        return null;
    }

    //    | '~' singleExpression                                                   # BitNotExpression
    @Override
    public Void visitBitNotExpression(BitNotExpressionContext stsBitNotExpression) {
        preOperatorExpressionWrite("~", stsBitNotExpression.singleExpression());
        return null;
    }

    //    | '!' singleExpression                                                   # NotExpression
    @Override
    public Void visitNotExpression(NotExpressionContext stsNotExpression) {
        preOperatorExpressionWrite("!", stsNotExpression.singleExpression());
        return null;
    }

    //    | singleExpression ('*' | '/' | '%') singleExpression                    # MultiplicativeExpression
    @Override
    public Void visitMultiplicativeExpression(MultiplicativeExpressionContext stsMultiplicativeExpression) {
        String op = stsMultiplicativeExpression.getChild(1).getText();
        binaryOperatorWrite(op, stsMultiplicativeExpression.singleExpression());
        return null;
    }

    //    | singleExpression ('+' | '-') singleExpression                          # AdditiveExpression
    @Override
    public Void visitAdditiveExpression(AdditiveExpressionContext stsAdditiveExpression) {
        String op = stsAdditiveExpression.getChild(1).getText();
        binaryOperatorWrite(op, stsAdditiveExpression.singleExpression());
        return null;
    }

    //    | singleExpression ('<' '<' | '>' '>' | '>' '>' '>') singleExpression                # BitShiftExpression
    @Override
    public Void visitBitShiftExpression(BitShiftExpressionContext stsBitShiftExpression) {
        String op = stsBitShiftExpression.shiftOperator().getText();
        binaryOperatorWrite(op, stsBitShiftExpression.singleExpression());
        return null;
    }

    //    | singleExpression ('<' | '>' | '<=' | '>=') singleExpression            # RelationalExpression
    @Override
    public Void visitRelationalExpression(RelationalExpressionContext stsRelationalExpression) {
        String op = stsRelationalExpression.getChild(1).getText();
        binaryOperatorWrite(op, stsRelationalExpression.singleExpression());
        return null;
    }

    // | singleExpression ('==' | '!=') singleExpression                        # EqualityExpression
    @Override
    public Void visitEqualityExpression(EqualityExpressionContext stsEqualityExpression) {
        String op = stsEqualityExpression.getChild(1).getText();
        binaryOperatorWrite(op, stsEqualityExpression.singleExpression());
        return null;
    }

    //    | singleExpression '&' singleExpression                                  # BitAndExpression
    @Override
    public Void visitBitAndExpression(BitAndExpressionContext stsBitAndExpression) {
        binaryOperatorWrite(stsBitAndExpression.BitAnd().getText(), stsBitAndExpression.singleExpression());
        return null;
    }

    //    | singleExpression '&' singleExpression                                  # BitAndExpression
    @Override
    public Void visitBitXOrExpression(BitXOrExpressionContext stsBitXOrExpression) {
        binaryOperatorWrite(stsBitXOrExpression.BitXor().getText(), stsBitXOrExpression.singleExpression());
        return null;
    }

    //    | singleExpression '|' singleExpression                                  # BitOrExpression
    @Override
    public Void visitBitOrExpression(BitOrExpressionContext stsBitOrExpression) {
        binaryOperatorWrite(stsBitOrExpression.BitOr().getText(), stsBitOrExpression.singleExpression());
        return null;
    }

    //    | singleExpression '&&' singleExpression                                 # LogicalAndExpression
    @Override
    public Void visitLogicalAndExpression(LogicalAndExpressionContext stsLogicalAndExpression) {
        binaryOperatorWrite(stsLogicalAndExpression.And().getText(), stsLogicalAndExpression.singleExpression());
        return null;
    }

    //    | singleExpression '||' singleExpression                                 # LogicalOrExpression
    @Override
    public Void visitLogicalOrExpression(LogicalOrExpressionContext stsLogicalOrExpression) {
        binaryOperatorWrite(stsLogicalOrExpression.Or().getText(), stsLogicalOrExpression.singleExpression());
        return null;
    }

    //    | singleExpression '?' singleExpression ':' singleExpression             # TernaryExpression
    @Override
    public Void visitTernaryExpression(TernaryExpressionContext stsTernaryExpression) {
        List<SingleExpressionContext> stsExpressionList = stsTernaryExpression.singleExpression();

        stsExpressionList.get(0).accept(this);
        sb.append(" ? ");
        stsExpressionList.get(1).accept(this);
        sb.append(" : ");
        stsExpressionList.get(2).accept(this);

        return null;
    }

    //    | singleExpression '=' singleExpression                                  # AssignmentExpression
    @Override
    public Void visitAssignmentExpression(AssignmentExpressionContext sstAssignmentExpression) {
        binaryOperatorWrite(sstAssignmentExpression.Assign().getText(), sstAssignmentExpression.singleExpression());
        return null;
    }

    //    | singleExpression assignmentOperator singleExpression                   # AssignmentOperatorExpression
    @Override
    public Void visitAssignmentOperatorExpression(AssignmentOperatorExpressionContext stsAssignmentOperatorExpression) {
        String op = stsAssignmentOperatorExpression.assignmentOperator().getText();
        binaryOperatorWrite(op, stsAssignmentOperatorExpression.singleExpression());
        return null;
    }

        //    | singleExpression '[' indexExpression ']' # ArrayAccessExpression
    @Override
    public Void visitArrayAccessExpression(ArrayAccessExpressionContext stsArrayAccessExpression) {
        stsArrayAccessExpression.singleExpression().accept(this);

        IndexExpressionContext stsIndexExpression = stsArrayAccessExpression.indexExpression();
        assert(stsIndexExpression != null);
        visitIndexExpression(stsIndexExpression);

        return null;
    }

    // lambdaExpression: : '(' formalParameterList? ')' typeAnnotation Arrow lambdaBody           # LambdaExpression   // ECMAScript 6
    @Override
    public Void visitLambdaExpression(LambdaExpressionContext stsLambdaExpression) {
        sb.append('(');
        ParameterListContext stsParameterList = stsLambdaExpression.parameterList();
        if (stsParameterList != null) {
            visitParameterList(stsParameterList);
        }
        sb.append(')');

        visitTypeAnnotation(stsLambdaExpression.typeAnnotation());

        sb.append(' ').append(stsLambdaExpression.Arrow().getText());

        visitLambdaBody(stsLambdaExpression.lambdaBody());

        return null;
    }

    // lambdaBody
    //    : singleExpression
    //    | '{' functionBody '}'
    @Override
    public Void visitLambdaBody(LambdaBodyContext stsLambdaBody) {
        SingleExpressionContext stsSingleExpression = stsLambdaBody.singleExpression();
        if (stsSingleExpression != null) {
            stsSingleExpression.accept(this);
        }
        else {
            BlockContext stsBlock = stsLambdaBody.block();
            assert(stsBlock != null);

            if (!doNeededIndent()) {
                sb.append(' ');
            }

            visitBlock(stsBlock);
        }

        return null;
    }

    // literal:
    //    NullLiteral
    //  | BooleanLiteral
    //  | StringLiteral
    //  | numericLiteral
    @Override
    public Void visitLiteral(LiteralContext stsLiteral) {
        sb.append(stsLiteral.getText());

        return null;
    }

    // deferStatement: Defer statement;
    @Override
    public Void visitDeferStatement(DeferStatementContext stsDeferStatement) {
        doNeededIndent();

        sb.append(stsDeferStatement.Defer().getText()).append(' ');
        stsDeferStatement.statement().accept(this);

        return null;
    }

    // Try singleExpression # TryExpression
    @Override
    public Void visitTryExpression(TryExpressionContext stsTryExpression) {
        sb.append(stsTryExpression.Try()).append(' ');
        stsTryExpression.singleExpression().accept(this);

        return null;
    }
}
