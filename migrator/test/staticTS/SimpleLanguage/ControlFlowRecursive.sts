/*
 * Copyright (c) 2022-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import utils.Consumer;

export class ControlFlowRecursive {
  static const n1: int = 3;
  static const n2: int = 5;
  static const expected: int = 57775;
  private static ack(m: int, n: int): int {
    if (m == 0) {
      return n + 1;
    }
    if (n == 0) {
      return ack(m - 1, 1);
    }
    return ack(m - 1, ack(m, n - 1));
  }

  private static fib(n: int): int {
    if (n < 2) {
      return 1;
    }
    return fib(n - 2) + fib(n - 1);
  }

  private static tak(x: int, y: int, z: int): int {
    if (y >= x) {
      return z;
    }
    return tak(tak(x - 1, y, z), tak(y - 1, z, x), tak(z - 1, x, y));
  }

  public static run(): void {
    let result: int = 0;
    for (let j: int = this.n1; j <= this.n2; ++j) {
      result += ack(3, j);      result += fib(17 + j);
      result += tak(3 * j + 3, 2 * j + 2, j + 1);
    }
    if (result != this.expected) {
      //System.err.println("ERROR: bad result: expected " + expected + " but got " + result);
      //System.exit(-1);
    }
    Consumer.consumeInt(result);
  }
}

