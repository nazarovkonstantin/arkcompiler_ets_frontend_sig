/*
 * Copyright (c) 2022-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */ 
package com.ohos.migrator.test.java;

open class Seq<T> {
    head : T ;
    tail : Seq<T> ;
    constructor() {
        this(null, null);
    }

    constructor(head : T, tail : Seq<T>) {
        this.head = head;
        this.tail = tail;
    }

    open isEmpty(): boolean {
        return tail == null;
    }
    open class Zipper<S> {
        open zip(that : Seq<S>): Seq<Pair<T, S>> {
            if (isEmpty() || that.isEmpty()) {
                return new Seq<Pair<T, S>>();
            }
            else {
                let tailZipper : Seq<T>.Zipper<S> = new tail.Zipper<S>();
                return new Seq<Pair<T, S>>(new Pair<T, S>(head, that.head), tailZipper.zip(that.tail));
            }
        }
    }

}

open class Pair<T, S> {
    fst : T ;
    snd : S ;
    constructor(f : T, s : S) {
        fst = f;
        snd = s;
    }

}

open class Test  {
    public static main(args : String[]): void {
        let strs : Seq<String> = new Seq<String>("a", new Seq<String>("b", new Seq<String>()));
        let nums : Seq<Number> = new Seq<Number>(new Integer(1), new Seq<Number>(new Double(1.5), new Seq<Number>()));
        let zipper : Seq<String>.Zipper<Number> = new strs.Zipper<Number>();
        let combined : Seq<Pair<String, Number>> = zipper.zip(nums);
    }
}

