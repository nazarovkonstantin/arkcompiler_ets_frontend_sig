/*
 * Copyright (c) 2022-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.ohos.migrator.test.java;

export class test_enum extends Enum<test_enum>  {
    public static values(): test_enum[] {
        return [];
    }
    public static valueOf(name : String): test_enum {
        for (let value : test_enum of values()){
            if (name == value.toString()) return value;
        }
        return null;
    }
    private constructor(name : String, ordinal : int) {
        super(name, ordinal);
    }

}
class Planet extends Enum<Planet>  {
    public static const MERCURY : Planet = new Planet("MERCURY", 0);
    public static const VENUS : Planet = new Planet("VENUS", 1);
    public static const EARTH : Planet = new Planet("EARTH", 2);
    public static const MARS : Planet = new Planet("MARS", 3);
    public static const JUPITER : Planet = new Planet("JUPITER", 4);
    public static const SATURN : Planet = new Planet("SATURN", 5);
    public static const URANUS : Planet = new Planet("URANUS", 6);
    public static const NEPTUNE : Planet = new Planet("NEPTUNE", 7);

    public static values(): Planet[] {
        return [MERCURY, VENUS, EARTH, MARS, JUPITER, SATURN, URANUS, NEPTUNE];
    }
    public static valueOf(name : String): Planet {
        for (let value : Planet of values()){
            if (name == value.toString()) return value;
        }
        return null;
    }
    private constructor(name : String, ordinal : int) {
        super(name, ordinal);
    }
}
open class NestedEnums  {
    private static class Colors extends Enum<Colors>  {
        public static const RED : Colors = new Colors("RED", 0);
        public static const GREEN : Colors = new Colors("GREEN", 1);
        public static const BLUE : Colors = new Colors("BLUE", 2);

        public static values(): Colors[] {
            return [RED, GREEN, BLUE];
        }
        public static valueOf(name : String): Colors {
            for (let value : Colors of values()){
                if (name == value.toString()) return value;
            }
            return null;
        }
        private constructor(name : String, ordinal : int) {
            super(name, ordinal);
        }
    }
    protected static class Size extends Enum<Size>  {
        public static const SMALL : Size = new Size("SMALL", 0);
        public static const MEDIUM : Size = new Size("MEDIUM", 1);
        public static const LARGE : Size = new Size("LARGE", 2);

        public static values(): Size[] {
            return [SMALL, MEDIUM, LARGE];
        }
        public static valueOf(name : String): Size {
            for (let value : Size of values()){
                if (name == value.toString()) return value;
            }
            return null;
        }
        private constructor(name : String, ordinal : int) {
            super(name, ordinal);
        }
    }
    public static class Direction extends Enum<Direction>  {
        public static const NORTH : Direction = new Direction("NORTH", 0);
        public static const EAST : Direction = new Direction("EAST", 1);
        public static const SOUTH : Direction = new Direction("SOUTH", 2);
        public static const WEST : Direction = new Direction("WEST", 3);

        public static values(): Direction[] {
            return [NORTH, EAST, SOUTH, WEST];
        }
        public static valueOf(name : String): Direction {
            for (let value : Direction of values()){
                if (name == value.toString()) return value;
            }
            return null;
        }
        private constructor(name : String, ordinal : int) {
            super(name, ordinal);
        }
    }
    static class Operator extends Enum<Operator>  {
        public static const PLUS : Operator = new Operator("PLUS", 0);
        public static const MINUS : Operator = new Operator("MINUS", 1);
        public static const MULTIPLY : Operator = new Operator("MULTIPLY", 2);
        public static const DIVIDE : Operator = new Operator("DIVIDE", 3);

        public static values(): Operator[] {
            return [PLUS, MINUS, MULTIPLY, DIVIDE];
        }
        public static valueOf(name : String): Operator {
            for (let value : Operator of values()){
                if (name == value.toString()) return value;
            }
            return null;
        }
        private constructor(name : String, ordinal : int) {
            super(name, ordinal);
        }
    }
}

