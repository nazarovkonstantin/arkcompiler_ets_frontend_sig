{
  "type": "Program",
  "statements": [
    {
      "type": "VariableDeclaration",
      "declarations": [
        {
          "type": "VariableDeclarator",
          "id": {
            "type": "Identifier",
            "name": "a",
            "typeAnnotation": {
              "type": "TSBigIntKeyword",
              "loc": {
                "start": {
                  "line": 1,
                  "column": 8
                },
                "end": {
                  "line": 1,
                  "column": 14
                }
              }
            },
            "decorators": [],
            "loc": {
              "start": {
                "line": 1,
                "column": 5
              },
              "end": {
                "line": 1,
                "column": 6
              }
            }
          },
          "init": null,
          "loc": {
            "start": {
              "line": 1,
              "column": 5
            },
            "end": {
              "line": 1,
              "column": 6
            }
          }
        }
      ],
      "kind": "var",
      "loc": {
        "start": {
          "line": 1,
          "column": 1
        },
        "end": {
          "line": 1,
          "column": 15
        }
      }
    },
    {
      "type": "VariableDeclaration",
      "declarations": [
        {
          "type": "VariableDeclarator",
          "id": {
            "type": "Identifier",
            "name": "b",
            "typeAnnotation": {
              "type": "TSAnyKeyword",
              "loc": {
                "start": {
                  "line": 2,
                  "column": 8
                },
                "end": {
                  "line": 2,
                  "column": 11
                }
              }
            },
            "decorators": [],
            "loc": {
              "start": {
                "line": 2,
                "column": 5
              },
              "end": {
                "line": 2,
                "column": 6
              }
            }
          },
          "init": null,
          "loc": {
            "start": {
              "line": 2,
              "column": 5
            },
            "end": {
              "line": 2,
              "column": 6
            }
          }
        }
      ],
      "kind": "var",
      "loc": {
        "start": {
          "line": 2,
          "column": 1
        },
        "end": {
          "line": 2,
          "column": 12
        }
      }
    },
    {
      "type": "ExpressionStatement",
      "expression": {
        "type": "BinaryExpression",
        "operator": "in",
        "left": {
          "type": "Identifier",
          "name": "a",
          "decorators": [],
          "loc": {
            "start": {
              "line": 3,
              "column": 1
            },
            "end": {
              "line": 3,
              "column": 2
            }
          }
        },
        "right": {
          "type": "Identifier",
          "name": "b",
          "decorators": [],
          "loc": {
            "start": {
              "line": 3,
              "column": 6
            },
            "end": {
              "line": 3,
              "column": 7
            }
          }
        },
        "loc": {
          "start": {
            "line": 3,
            "column": 1
          },
          "end": {
            "line": 3,
            "column": 7
          }
        }
      },
      "loc": {
        "start": {
          "line": 3,
          "column": 1
        },
        "end": {
          "line": 3,
          "column": 8
        }
      }
    }
  ],
  "loc": {
    "start": {
      "line": 1,
      "column": 1
    },
    "end": {
      "line": 4,
      "column": 1
    }
  }
}
TypeError: The left-hand side of an 'in' expression must be of type 'any', 'string', 'number', or 'symbol'. [in_5.ts:3:1]
