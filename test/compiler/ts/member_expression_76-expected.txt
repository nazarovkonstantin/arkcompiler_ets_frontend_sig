{
  "type": "Program",
  "statements": [
    {
      "type": "TSInterfaceDeclaration",
      "body": {
        "type": "TSInterfaceBody",
        "body": [
          {
            "type": "TSPropertySignature",
            "computed": false,
            "optional": false,
            "readonly": true,
            "key": {
              "type": "Identifier",
              "name": "c",
              "decorators": [],
              "loc": {
                "start": {
                  "line": 2,
                  "column": 14
                },
                "end": {
                  "line": 2,
                  "column": 15
                }
              }
            },
            "typeAnnotation": {
              "type": "TSNumberKeyword",
              "loc": {
                "start": {
                  "line": 2,
                  "column": 17
                },
                "end": {
                  "line": 2,
                  "column": 23
                }
              }
            },
            "loc": {
              "start": {
                "line": 2,
                "column": 5
              },
              "end": {
                "line": 2,
                "column": 24
              }
            }
          }
        ],
        "loc": {
          "start": {
            "line": 1,
            "column": 15
          },
          "end": {
            "line": 3,
            "column": 2
          }
        }
      },
      "id": {
        "type": "Identifier",
        "name": "foo",
        "decorators": [],
        "loc": {
          "start": {
            "line": 1,
            "column": 11
          },
          "end": {
            "line": 1,
            "column": 14
          }
        }
      },
      "extends": [],
      "loc": {
        "start": {
          "line": 1,
          "column": 1
        },
        "end": {
          "line": 3,
          "column": 2
        }
      }
    },
    {
      "type": "VariableDeclaration",
      "declarations": [
        {
          "type": "VariableDeclarator",
          "id": {
            "type": "Identifier",
            "name": "a",
            "typeAnnotation": {
              "type": "TSTypeLiteral",
              "members": [
                {
                  "type": "TSPropertySignature",
                  "computed": false,
                  "optional": false,
                  "readonly": false,
                  "key": {
                    "type": "Identifier",
                    "name": "b",
                    "decorators": [],
                    "loc": {
                      "start": {
                        "line": 5,
                        "column": 9
                      },
                      "end": {
                        "line": 5,
                        "column": 10
                      }
                    }
                  },
                  "typeAnnotation": {
                    "type": "TSTypeReference",
                    "typeName": {
                      "type": "Identifier",
                      "name": "foo",
                      "decorators": [],
                      "loc": {
                        "start": {
                          "line": 5,
                          "column": 12
                        },
                        "end": {
                          "line": 5,
                          "column": 15
                        }
                      }
                    },
                    "loc": {
                      "start": {
                        "line": 5,
                        "column": 12
                      },
                      "end": {
                        "line": 5,
                        "column": 15
                      }
                    }
                  },
                  "loc": {
                    "start": {
                      "line": 5,
                      "column": 9
                    },
                    "end": {
                      "line": 5,
                      "column": 16
                    }
                  }
                }
              ],
              "loc": {
                "start": {
                  "line": 5,
                  "column": 8
                },
                "end": {
                  "line": 5,
                  "column": 16
                }
              }
            },
            "decorators": [],
            "loc": {
              "start": {
                "line": 5,
                "column": 5
              },
              "end": {
                "line": 5,
                "column": 6
              }
            }
          },
          "init": null,
          "loc": {
            "start": {
              "line": 5,
              "column": 5
            },
            "end": {
              "line": 5,
              "column": 6
            }
          }
        }
      ],
      "kind": "var",
      "loc": {
        "start": {
          "line": 5,
          "column": 1
        },
        "end": {
          "line": 5,
          "column": 17
        }
      }
    },
    {
      "type": "ExpressionStatement",
      "expression": {
        "type": "AssignmentExpression",
        "operator": "=",
        "left": {
          "type": "MemberExpression",
          "object": {
            "type": "MemberExpression",
            "object": {
              "type": "Identifier",
              "name": "a",
              "decorators": [],
              "loc": {
                "start": {
                  "line": 6,
                  "column": 1
                },
                "end": {
                  "line": 6,
                  "column": 2
                }
              }
            },
            "property": {
              "type": "Identifier",
              "name": "b",
              "decorators": [],
              "loc": {
                "start": {
                  "line": 6,
                  "column": 3
                },
                "end": {
                  "line": 6,
                  "column": 4
                }
              }
            },
            "computed": false,
            "optional": false,
            "loc": {
              "start": {
                "line": 6,
                "column": 1
              },
              "end": {
                "line": 6,
                "column": 4
              }
            }
          },
          "property": {
            "type": "Identifier",
            "name": "c",
            "decorators": [],
            "loc": {
              "start": {
                "line": 6,
                "column": 5
              },
              "end": {
                "line": 6,
                "column": 6
              }
            }
          },
          "computed": false,
          "optional": false,
          "loc": {
            "start": {
              "line": 6,
              "column": 1
            },
            "end": {
              "line": 6,
              "column": 6
            }
          }
        },
        "right": {
          "type": "NumberLiteral",
          "value": 5,
          "loc": {
            "start": {
              "line": 6,
              "column": 9
            },
            "end": {
              "line": 6,
              "column": 10
            }
          }
        },
        "loc": {
          "start": {
            "line": 6,
            "column": 1
          },
          "end": {
            "line": 6,
            "column": 10
          }
        }
      },
      "loc": {
        "start": {
          "line": 6,
          "column": 1
        },
        "end": {
          "line": 6,
          "column": 11
        }
      }
    }
  ],
  "loc": {
    "start": {
      "line": 1,
      "column": 1
    },
    "end": {
      "line": 7,
      "column": 1
    }
  }
}
TypeError: Cannot assign to 'c' because it is a read-only property. [member_expression_76.ts:6:5]
