{
  "type": "Program",
  "statements": [
    {
      "type": "VariableDeclaration",
      "declarations": [
        {
          "type": "VariableDeclarator",
          "id": {
            "type": "Identifier",
            "name": "a",
            "typeAnnotation": {
              "type": "TSUnionType",
              "types": [
                {
                  "type": "TSTupleType",
                  "elementTypes": [
                    {
                      "type": "TSNumberKeyword",
                      "loc": {
                        "start": {
                          "line": 1,
                          "column": 9
                        },
                        "end": {
                          "line": 1,
                          "column": 16
                        }
                      }
                    },
                    {
                      "type": "TSNumberKeyword",
                      "loc": {
                        "start": {
                          "line": 1,
                          "column": 17
                        },
                        "end": {
                          "line": 1,
                          "column": 24
                        }
                      }
                    }
                  ],
                  "loc": {
                    "start": {
                      "line": 1,
                      "column": 8
                    },
                    "end": {
                      "line": 1,
                      "column": 24
                    }
                  }
                },
                {
                  "type": "TSTupleType",
                  "elementTypes": [
                    {
                      "type": "TSNumberKeyword",
                      "loc": {
                        "start": {
                          "line": 1,
                          "column": 28
                        },
                        "end": {
                          "line": 1,
                          "column": 35
                        }
                      }
                    },
                    {
                      "type": "TSNumberKeyword",
                      "loc": {
                        "start": {
                          "line": 1,
                          "column": 36
                        },
                        "end": {
                          "line": 1,
                          "column": 43
                        }
                      }
                    },
                    {
                      "type": "TSNumberKeyword",
                      "loc": {
                        "start": {
                          "line": 1,
                          "column": 44
                        },
                        "end": {
                          "line": 1,
                          "column": 51
                        }
                      }
                    }
                  ],
                  "loc": {
                    "start": {
                      "line": 1,
                      "column": 27
                    },
                    "end": {
                      "line": 1,
                      "column": 51
                    }
                  }
                }
              ],
              "loc": {
                "start": {
                  "line": 1,
                  "column": 8
                },
                "end": {
                  "line": 1,
                  "column": 51
                }
              }
            },
            "decorators": [],
            "loc": {
              "start": {
                "line": 1,
                "column": 5
              },
              "end": {
                "line": 1,
                "column": 6
              }
            }
          },
          "init": {
            "type": "ArrayExpression",
            "elements": [
              {
                "type": "NumberLiteral",
                "value": 1,
                "loc": {
                  "start": {
                    "line": 1,
                    "column": 55
                  },
                  "end": {
                    "line": 1,
                    "column": 56
                  }
                }
              }
            ],
            "loc": {
              "start": {
                "line": 1,
                "column": 54
              },
              "end": {
                "line": 1,
                "column": 57
              }
            }
          },
          "loc": {
            "start": {
              "line": 1,
              "column": 5
            },
            "end": {
              "line": 1,
              "column": 57
            }
          }
        }
      ],
      "kind": "var",
      "loc": {
        "start": {
          "line": 1,
          "column": 1
        },
        "end": {
          "line": 1,
          "column": 58
        }
      }
    }
  ],
  "loc": {
    "start": {
      "line": 1,
      "column": 1
    },
    "end": {
      "line": 1,
      "column": 58
    }
  }
}
TypeError: Type '[number]' is not assignable to type '[number, number] | [number, number, number]'. [tupleAssignability21.ts:1:5]
