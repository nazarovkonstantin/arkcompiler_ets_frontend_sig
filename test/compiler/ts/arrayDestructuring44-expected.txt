{
  "type": "Program",
  "statements": [
    {
      "type": "VariableDeclaration",
      "declarations": [
        {
          "type": "VariableDeclarator",
          "id": {
            "type": "Identifier",
            "name": "a",
            "typeAnnotation": {
              "type": "TSArrayType",
              "elementType": {
                "type": "TSStringKeyword",
                "loc": {
                  "start": {
                    "line": 1,
                    "column": 8
                  },
                  "end": {
                    "line": 1,
                    "column": 14
                  }
                }
              },
              "loc": {
                "start": {
                  "line": 1,
                  "column": 8
                },
                "end": {
                  "line": 1,
                  "column": 16
                }
              }
            },
            "decorators": [],
            "loc": {
              "start": {
                "line": 1,
                "column": 5
              },
              "end": {
                "line": 1,
                "column": 6
              }
            }
          },
          "init": null,
          "loc": {
            "start": {
              "line": 1,
              "column": 5
            },
            "end": {
              "line": 1,
              "column": 6
            }
          }
        }
      ],
      "kind": "var",
      "loc": {
        "start": {
          "line": 1,
          "column": 1
        },
        "end": {
          "line": 1,
          "column": 17
        }
      }
    },
    {
      "type": "VariableDeclaration",
      "declarations": [
        {
          "type": "VariableDeclarator",
          "id": {
            "type": "Identifier",
            "name": "b",
            "typeAnnotation": {
              "type": "TSArrayType",
              "elementType": {
                "type": "TSStringKeyword",
                "loc": {
                  "start": {
                    "line": 2,
                    "column": 8
                  },
                  "end": {
                    "line": 2,
                    "column": 14
                  }
                }
              },
              "loc": {
                "start": {
                  "line": 2,
                  "column": 8
                },
                "end": {
                  "line": 2,
                  "column": 16
                }
              }
            },
            "decorators": [],
            "loc": {
              "start": {
                "line": 2,
                "column": 5
              },
              "end": {
                "line": 2,
                "column": 6
              }
            }
          },
          "init": null,
          "loc": {
            "start": {
              "line": 2,
              "column": 5
            },
            "end": {
              "line": 2,
              "column": 6
            }
          }
        }
      ],
      "kind": "var",
      "loc": {
        "start": {
          "line": 2,
          "column": 1
        },
        "end": {
          "line": 2,
          "column": 17
        }
      }
    },
    {
      "type": "ExpressionStatement",
      "expression": {
        "type": "AssignmentExpression",
        "operator": "=",
        "left": {
          "type": "ArrayPattern",
          "elements": [
            {
              "type": "ArrayPattern",
              "elements": [
                {
                  "type": "RestElement",
                  "argument": {
                    "type": "Identifier",
                    "name": "b",
                    "decorators": [],
                    "loc": {
                      "start": {
                        "line": 4,
                        "column": 6
                      },
                      "end": {
                        "line": 4,
                        "column": 7
                      }
                    }
                  },
                  "loc": {
                    "start": {
                      "line": 4,
                      "column": 3
                    },
                    "end": {
                      "line": 4,
                      "column": 7
                    }
                  }
                }
              ],
              "loc": {
                "start": {
                  "line": 4,
                  "column": 2
                },
                "end": {
                  "line": 4,
                  "column": 8
                }
              }
            },
            {
              "type": "RestElement",
              "argument": {
                "type": "Identifier",
                "name": "a",
                "decorators": [],
                "loc": {
                  "start": {
                    "line": 4,
                    "column": 13
                  },
                  "end": {
                    "line": 4,
                    "column": 14
                  }
                }
              },
              "loc": {
                "start": {
                  "line": 4,
                  "column": 10
                },
                "end": {
                  "line": 4,
                  "column": 14
                }
              }
            }
          ],
          "loc": {
            "start": {
              "line": 4,
              "column": 1
            },
            "end": {
              "line": 4,
              "column": 15
            }
          }
        },
        "right": {
          "type": "ArrayExpression",
          "elements": [
            {
              "type": "ArrayExpression",
              "elements": [
                {
                  "type": "StringLiteral",
                  "value": "",
                  "loc": {
                    "start": {
                      "line": 4,
                      "column": 20
                    },
                    "end": {
                      "line": 4,
                      "column": 25
                    }
                  }
                },
                {
                  "type": "BooleanLiteral",
                  "value": false,
                  "loc": {
                    "start": {
                      "line": 4,
                      "column": 27
                    },
                    "end": {
                      "line": 4,
                      "column": 32
                    }
                  }
                }
              ],
              "loc": {
                "start": {
                  "line": 4,
                  "column": 19
                },
                "end": {
                  "line": 4,
                  "column": 33
                }
              }
            },
            {
              "type": "StringLiteral",
              "value": "",
              "loc": {
                "start": {
                  "line": 4,
                  "column": 35
                },
                "end": {
                  "line": 4,
                  "column": 40
                }
              }
            }
          ],
          "loc": {
            "start": {
              "line": 4,
              "column": 18
            },
            "end": {
              "line": 4,
              "column": 41
            }
          }
        },
        "loc": {
          "start": {
            "line": 4,
            "column": 1
          },
          "end": {
            "line": 4,
            "column": 41
          }
        }
      },
      "loc": {
        "start": {
          "line": 4,
          "column": 1
        },
        "end": {
          "line": 4,
          "column": 41
        }
      }
    }
  ],
  "loc": {
    "start": {
      "line": 1,
      "column": 1
    },
    "end": {
      "line": 4,
      "column": 41
    }
  }
}
TypeError: Type '(string | boolean)[]' is not assignable to type 'string[]'. [arrayDestructuring44.ts:4:6]
