{
  "type": "Program",
  "statements": [
    {
      "type": "FunctionDeclaration",
      "function": {
        "type": "ScriptFunction",
        "id": {
          "type": "Identifier",
          "name": "func",
          "decorators": [],
          "loc": {
            "start": {
              "line": 1,
              "column": 10
            },
            "end": {
              "line": 1,
              "column": 14
            }
          }
        },
        "generator": false,
        "async": false,
        "expression": false,
        "params": [
          {
            "type": "Identifier",
            "name": "a",
            "typeAnnotation": {
              "type": "TSTypeLiteral",
              "members": [
                {
                  "type": "TSPropertySignature",
                  "computed": false,
                  "optional": false,
                  "readonly": false,
                  "key": {
                    "type": "Identifier",
                    "name": "a",
                    "decorators": [],
                    "loc": {
                      "start": {
                        "line": 1,
                        "column": 20
                      },
                      "end": {
                        "line": 1,
                        "column": 21
                      }
                    }
                  },
                  "typeAnnotation": {
                    "type": "TSNumberKeyword",
                    "loc": {
                      "start": {
                        "line": 1,
                        "column": 23
                      },
                      "end": {
                        "line": 1,
                        "column": 29
                      }
                    }
                  },
                  "loc": {
                    "start": {
                      "line": 1,
                      "column": 20
                    },
                    "end": {
                      "line": 1,
                      "column": 30
                    }
                  }
                },
                {
                  "type": "TSPropertySignature",
                  "computed": false,
                  "optional": false,
                  "readonly": false,
                  "key": {
                    "type": "Identifier",
                    "name": "b",
                    "decorators": [],
                    "loc": {
                      "start": {
                        "line": 1,
                        "column": 31
                      },
                      "end": {
                        "line": 1,
                        "column": 32
                      }
                    }
                  },
                  "typeAnnotation": {
                    "type": "TSStringKeyword",
                    "loc": {
                      "start": {
                        "line": 1,
                        "column": 34
                      },
                      "end": {
                        "line": 1,
                        "column": 40
                      }
                    }
                  },
                  "loc": {
                    "start": {
                      "line": 1,
                      "column": 31
                    },
                    "end": {
                      "line": 1,
                      "column": 42
                    }
                  }
                }
              ],
              "loc": {
                "start": {
                  "line": 1,
                  "column": 18
                },
                "end": {
                  "line": 1,
                  "column": 42
                }
              }
            },
            "decorators": [],
            "loc": {
              "start": {
                "line": 1,
                "column": 15
              },
              "end": {
                "line": 1,
                "column": 16
              }
            }
          },
          {
            "type": "Identifier",
            "name": "b",
            "typeAnnotation": {
              "type": "TSNumberKeyword",
              "loc": {
                "start": {
                  "line": 1,
                  "column": 47
                },
                "end": {
                  "line": 1,
                  "column": 53
                }
              }
            },
            "decorators": [],
            "loc": {
              "start": {
                "line": 1,
                "column": 44
              },
              "end": {
                "line": 1,
                "column": 45
              }
            }
          }
        ],
        "returnType": {
          "type": "TSTypeLiteral",
          "members": [
            {
              "type": "TSPropertySignature",
              "computed": false,
              "optional": false,
              "readonly": false,
              "key": {
                "type": "Identifier",
                "name": "a",
                "decorators": [],
                "loc": {
                  "start": {
                    "line": 1,
                    "column": 58
                  },
                  "end": {
                    "line": 1,
                    "column": 59
                  }
                }
              },
              "typeAnnotation": {
                "type": "TSArrayType",
                "elementType": {
                  "type": "TSNumberKeyword",
                  "loc": {
                    "start": {
                      "line": 1,
                      "column": 61
                    },
                    "end": {
                      "line": 1,
                      "column": 67
                    }
                  }
                },
                "loc": {
                  "start": {
                    "line": 1,
                    "column": 61
                  },
                  "end": {
                    "line": 1,
                    "column": 69
                  }
                }
              },
              "loc": {
                "start": {
                  "line": 1,
                  "column": 58
                },
                "end": {
                  "line": 1,
                  "column": 70
                }
              }
            },
            {
              "type": "TSPropertySignature",
              "computed": false,
              "optional": false,
              "readonly": false,
              "key": {
                "type": "Identifier",
                "name": "b",
                "decorators": [],
                "loc": {
                  "start": {
                    "line": 1,
                    "column": 71
                  },
                  "end": {
                    "line": 1,
                    "column": 72
                  }
                }
              },
              "typeAnnotation": {
                "type": "TSBooleanKeyword",
                "loc": {
                  "start": {
                    "line": 1,
                    "column": 74
                  },
                  "end": {
                    "line": 1,
                    "column": 81
                  }
                }
              },
              "loc": {
                "start": {
                  "line": 1,
                  "column": 71
                },
                "end": {
                  "line": 1,
                  "column": 83
                }
              }
            }
          ],
          "loc": {
            "start": {
              "line": 1,
              "column": 56
            },
            "end": {
              "line": 1,
              "column": 83
            }
          }
        },
        "body": {
          "type": "BlockStatement",
          "statements": [
            {
              "type": "ReturnStatement",
              "argument": {
                "type": "ObjectExpression",
                "properties": [
                  {
                    "type": "Property",
                    "method": false,
                    "shorthand": false,
                    "computed": false,
                    "key": {
                      "type": "Identifier",
                      "name": "a",
                      "decorators": [],
                      "loc": {
                        "start": {
                          "line": 2,
                          "column": 14
                        },
                        "end": {
                          "line": 2,
                          "column": 15
                        }
                      }
                    },
                    "value": {
                      "type": "ArrayExpression",
                      "elements": [
                        {
                          "type": "NumberLiteral",
                          "value": 1,
                          "loc": {
                            "start": {
                              "line": 2,
                              "column": 18
                            },
                            "end": {
                              "line": 2,
                              "column": 19
                            }
                          }
                        },
                        {
                          "type": "NumberLiteral",
                          "value": 2,
                          "loc": {
                            "start": {
                              "line": 2,
                              "column": 21
                            },
                            "end": {
                              "line": 2,
                              "column": 22
                            }
                          }
                        },
                        {
                          "type": "NumberLiteral",
                          "value": 3,
                          "loc": {
                            "start": {
                              "line": 2,
                              "column": 24
                            },
                            "end": {
                              "line": 2,
                              "column": 25
                            }
                          }
                        }
                      ],
                      "loc": {
                        "start": {
                          "line": 2,
                          "column": 17
                        },
                        "end": {
                          "line": 2,
                          "column": 26
                        }
                      }
                    },
                    "kind": "init",
                    "loc": {
                      "start": {
                        "line": 2,
                        "column": 14
                      },
                      "end": {
                        "line": 2,
                        "column": 26
                      }
                    }
                  },
                  {
                    "type": "Property",
                    "method": false,
                    "shorthand": false,
                    "computed": false,
                    "key": {
                      "type": "Identifier",
                      "name": "b",
                      "decorators": [],
                      "loc": {
                        "start": {
                          "line": 2,
                          "column": 28
                        },
                        "end": {
                          "line": 2,
                          "column": 29
                        }
                      }
                    },
                    "value": {
                      "type": "BooleanLiteral",
                      "value": true,
                      "loc": {
                        "start": {
                          "line": 2,
                          "column": 31
                        },
                        "end": {
                          "line": 2,
                          "column": 35
                        }
                      }
                    },
                    "kind": "init",
                    "loc": {
                      "start": {
                        "line": 2,
                        "column": 28
                      },
                      "end": {
                        "line": 2,
                        "column": 35
                      }
                    }
                  }
                ],
                "loc": {
                  "start": {
                    "line": 2,
                    "column": 12
                  },
                  "end": {
                    "line": 2,
                    "column": 37
                  }
                }
              },
              "loc": {
                "start": {
                  "line": 2,
                  "column": 5
                },
                "end": {
                  "line": 2,
                  "column": 38
                }
              }
            }
          ],
          "loc": {
            "start": {
              "line": 1,
              "column": 84
            },
            "end": {
              "line": 3,
              "column": 2
            }
          }
        },
        "loc": {
          "start": {
            "line": 1,
            "column": 1
          },
          "end": {
            "line": 3,
            "column": 2
          }
        }
      },
      "loc": {
        "start": {
          "line": 1,
          "column": 1
        },
        "end": {
          "line": 3,
          "column": 2
        }
      }
    },
    {
      "type": "VariableDeclaration",
      "declarations": [
        {
          "type": "VariableDeclarator",
          "id": {
            "type": "Identifier",
            "name": "a",
            "typeAnnotation": {
              "type": "TSTypeLiteral",
              "members": [
                {
                  "type": "TSPropertySignature",
                  "computed": false,
                  "optional": false,
                  "readonly": false,
                  "key": {
                    "type": "Identifier",
                    "name": "a",
                    "decorators": [],
                    "loc": {
                      "start": {
                        "line": 5,
                        "column": 10
                      },
                      "end": {
                        "line": 5,
                        "column": 11
                      }
                    }
                  },
                  "typeAnnotation": {
                    "type": "TSArrayType",
                    "elementType": {
                      "type": "TSStringKeyword",
                      "loc": {
                        "start": {
                          "line": 5,
                          "column": 13
                        },
                        "end": {
                          "line": 5,
                          "column": 19
                        }
                      }
                    },
                    "loc": {
                      "start": {
                        "line": 5,
                        "column": 13
                      },
                      "end": {
                        "line": 5,
                        "column": 21
                      }
                    }
                  },
                  "loc": {
                    "start": {
                      "line": 5,
                      "column": 10
                    },
                    "end": {
                      "line": 5,
                      "column": 22
                    }
                  }
                },
                {
                  "type": "TSPropertySignature",
                  "computed": false,
                  "optional": false,
                  "readonly": false,
                  "key": {
                    "type": "Identifier",
                    "name": "b",
                    "decorators": [],
                    "loc": {
                      "start": {
                        "line": 5,
                        "column": 23
                      },
                      "end": {
                        "line": 5,
                        "column": 24
                      }
                    }
                  },
                  "typeAnnotation": {
                    "type": "TSBooleanKeyword",
                    "loc": {
                      "start": {
                        "line": 5,
                        "column": 26
                      },
                      "end": {
                        "line": 5,
                        "column": 33
                      }
                    }
                  },
                  "loc": {
                    "start": {
                      "line": 5,
                      "column": 23
                    },
                    "end": {
                      "line": 5,
                      "column": 35
                    }
                  }
                }
              ],
              "loc": {
                "start": {
                  "line": 5,
                  "column": 8
                },
                "end": {
                  "line": 5,
                  "column": 35
                }
              }
            },
            "decorators": [],
            "loc": {
              "start": {
                "line": 5,
                "column": 5
              },
              "end": {
                "line": 5,
                "column": 6
              }
            }
          },
          "init": {
            "type": "CallExpression",
            "callee": {
              "type": "Identifier",
              "name": "func",
              "decorators": [],
              "loc": {
                "start": {
                  "line": 5,
                  "column": 38
                },
                "end": {
                  "line": 5,
                  "column": 42
                }
              }
            },
            "arguments": [
              {
                "type": "ObjectExpression",
                "properties": [
                  {
                    "type": "Property",
                    "method": false,
                    "shorthand": false,
                    "computed": false,
                    "key": {
                      "type": "Identifier",
                      "name": "a",
                      "decorators": [],
                      "loc": {
                        "start": {
                          "line": 5,
                          "column": 45
                        },
                        "end": {
                          "line": 5,
                          "column": 46
                        }
                      }
                    },
                    "value": {
                      "type": "NumberLiteral",
                      "value": 5,
                      "loc": {
                        "start": {
                          "line": 5,
                          "column": 48
                        },
                        "end": {
                          "line": 5,
                          "column": 49
                        }
                      }
                    },
                    "kind": "init",
                    "loc": {
                      "start": {
                        "line": 5,
                        "column": 45
                      },
                      "end": {
                        "line": 5,
                        "column": 49
                      }
                    }
                  },
                  {
                    "type": "Property",
                    "method": false,
                    "shorthand": false,
                    "computed": false,
                    "key": {
                      "type": "Identifier",
                      "name": "b",
                      "decorators": [],
                      "loc": {
                        "start": {
                          "line": 5,
                          "column": 51
                        },
                        "end": {
                          "line": 5,
                          "column": 52
                        }
                      }
                    },
                    "value": {
                      "type": "StringLiteral",
                      "value": "",
                      "loc": {
                        "start": {
                          "line": 5,
                          "column": 54
                        },
                        "end": {
                          "line": 5,
                          "column": 59
                        }
                      }
                    },
                    "kind": "init",
                    "loc": {
                      "start": {
                        "line": 5,
                        "column": 51
                      },
                      "end": {
                        "line": 5,
                        "column": 59
                      }
                    }
                  }
                ],
                "loc": {
                  "start": {
                    "line": 5,
                    "column": 43
                  },
                  "end": {
                    "line": 5,
                    "column": 61
                  }
                }
              },
              {
                "type": "NumberLiteral",
                "value": 6,
                "loc": {
                  "start": {
                    "line": 5,
                    "column": 63
                  },
                  "end": {
                    "line": 5,
                    "column": 64
                  }
                }
              }
            ],
            "optional": false,
            "loc": {
              "start": {
                "line": 5,
                "column": 38
              },
              "end": {
                "line": 5,
                "column": 65
              }
            }
          },
          "loc": {
            "start": {
              "line": 5,
              "column": 5
            },
            "end": {
              "line": 5,
              "column": 65
            }
          }
        }
      ],
      "kind": "var",
      "loc": {
        "start": {
          "line": 5,
          "column": 1
        },
        "end": {
          "line": 5,
          "column": 66
        }
      }
    }
  ],
  "loc": {
    "start": {
      "line": 1,
      "column": 1
    },
    "end": {
      "line": 5,
      "column": 66
    }
  }
}
TypeError: Type '{ a: number[]; b: boolean; }' is not assignable to type '{ a: string[]; b: boolean; }' [objectLiteralAssignability18.ts:5:5]
