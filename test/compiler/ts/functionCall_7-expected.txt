{
  "type": "Program",
  "statements": [
    {
      "type": "FunctionDeclaration",
      "function": {
        "type": "ScriptFunction",
        "id": {
          "type": "Identifier",
          "name": "func",
          "decorators": [],
          "loc": {
            "start": {
              "line": 1,
              "column": 10
            },
            "end": {
              "line": 1,
              "column": 14
            }
          }
        },
        "generator": false,
        "async": false,
        "expression": false,
        "params": [
          {
            "type": "RestElement",
            "argument": {
              "type": "Identifier",
              "name": "a",
              "decorators": [],
              "loc": {
                "start": {
                  "line": 1,
                  "column": 18
                },
                "end": {
                  "line": 1,
                  "column": 19
                }
              }
            },
            "typeAnnotation": {
              "type": "TSArrayType",
              "elementType": {
                "type": "TSStringKeyword",
                "loc": {
                  "start": {
                    "line": 1,
                    "column": 21
                  },
                  "end": {
                    "line": 1,
                    "column": 27
                  }
                }
              },
              "loc": {
                "start": {
                  "line": 1,
                  "column": 21
                },
                "end": {
                  "line": 1,
                  "column": 29
                }
              }
            },
            "loc": {
              "start": {
                "line": 1,
                "column": 15
              },
              "end": {
                "line": 1,
                "column": 29
              }
            }
          }
        ],
        "body": {
          "type": "BlockStatement",
          "statements": [
            {
              "type": "ReturnStatement",
              "argument": {
                "type": "NumberLiteral",
                "value": 6,
                "loc": {
                  "start": {
                    "line": 2,
                    "column": 12
                  },
                  "end": {
                    "line": 2,
                    "column": 13
                  }
                }
              },
              "loc": {
                "start": {
                  "line": 2,
                  "column": 5
                },
                "end": {
                  "line": 2,
                  "column": 14
                }
              }
            }
          ],
          "loc": {
            "start": {
              "line": 1,
              "column": 31
            },
            "end": {
              "line": 3,
              "column": 2
            }
          }
        },
        "loc": {
          "start": {
            "line": 1,
            "column": 1
          },
          "end": {
            "line": 3,
            "column": 2
          }
        }
      },
      "loc": {
        "start": {
          "line": 1,
          "column": 1
        },
        "end": {
          "line": 3,
          "column": 2
        }
      }
    },
    {
      "type": "ExpressionStatement",
      "expression": {
        "type": "CallExpression",
        "callee": {
          "type": "Identifier",
          "name": "func",
          "decorators": [],
          "loc": {
            "start": {
              "line": 5,
              "column": 1
            },
            "end": {
              "line": 5,
              "column": 5
            }
          }
        },
        "arguments": [
          {
            "type": "NumberLiteral",
            "value": 2,
            "loc": {
              "start": {
                "line": 5,
                "column": 6
              },
              "end": {
                "line": 5,
                "column": 7
              }
            }
          }
        ],
        "optional": false,
        "loc": {
          "start": {
            "line": 5,
            "column": 1
          },
          "end": {
            "line": 5,
            "column": 8
          }
        }
      },
      "loc": {
        "start": {
          "line": 5,
          "column": 1
        },
        "end": {
          "line": 5,
          "column": 9
        }
      }
    }
  ],
  "loc": {
    "start": {
      "line": 1,
      "column": 1
    },
    "end": {
      "line": 5,
      "column": 9
    }
  }
}
TypeError: Argument of type 'number' is not assignable to parameter of type 'string'. [functionCall_7.ts:5:6]
