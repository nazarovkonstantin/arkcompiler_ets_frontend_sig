{
  "type": "Program",
  "statements": [
    {
      "type": "TSInterfaceDeclaration",
      "body": {
        "type": "TSInterfaceBody",
        "body": [],
        "loc": {
          "start": {
            "line": 1,
            "column": 13
          },
          "end": {
            "line": 1,
            "column": 15
          }
        }
      },
      "id": {
        "type": "Identifier",
        "name": "C",
        "decorators": [],
        "loc": {
          "start": {
            "line": 1,
            "column": 11
          },
          "end": {
            "line": 1,
            "column": 12
          }
        }
      },
      "extends": [],
      "loc": {
        "start": {
          "line": 1,
          "column": 1
        },
        "end": {
          "line": 1,
          "column": 15
        }
      }
    },
    {
      "type": "VariableDeclaration",
      "declarations": [
        {
          "type": "VariableDeclarator",
          "id": {
            "type": "Identifier",
            "name": "foo",
            "typeAnnotation": {
              "type": "TSAnyKeyword",
              "loc": {
                "start": {
                  "line": 2,
                  "column": 10
                },
                "end": {
                  "line": 2,
                  "column": 13
                }
              }
            },
            "decorators": [],
            "loc": {
              "start": {
                "line": 2,
                "column": 5
              },
              "end": {
                "line": 2,
                "column": 8
              }
            }
          },
          "init": null,
          "loc": {
            "start": {
              "line": 2,
              "column": 5
            },
            "end": {
              "line": 2,
              "column": 8
            }
          }
        }
      ],
      "kind": "var",
      "loc": {
        "start": {
          "line": 2,
          "column": 1
        },
        "end": {
          "line": 2,
          "column": 14
        }
      }
    },
    {
      "type": "VariableDeclaration",
      "declarations": [
        {
          "type": "VariableDeclarator",
          "id": {
            "type": "Identifier",
            "name": "bar",
            "decorators": [],
            "loc": {
              "start": {
                "line": 3,
                "column": 5
              },
              "end": {
                "line": 3,
                "column": 8
              }
            }
          },
          "init": {
            "type": "TSTypeAssertion",
            "typeAnnotation": {
              "type": "TSStringKeyword",
              "loc": {
                "start": {
                  "line": 3,
                  "column": 12
                },
                "end": {
                  "line": 3,
                  "column": 18
                }
              }
            },
            "expression": {
              "type": "Identifier",
              "name": "foo",
              "decorators": [],
              "loc": {
                "start": {
                  "line": 3,
                  "column": 20
                },
                "end": {
                  "line": 3,
                  "column": 23
                }
              }
            },
            "loc": {
              "start": {
                "line": 3,
                "column": 11
              },
              "end": {
                "line": 3,
                "column": 24
              }
            }
          },
          "loc": {
            "start": {
              "line": 3,
              "column": 5
            },
            "end": {
              "line": 3,
              "column": 24
            }
          }
        }
      ],
      "kind": "var",
      "loc": {
        "start": {
          "line": 3,
          "column": 1
        },
        "end": {
          "line": 3,
          "column": 24
        }
      }
    },
    {
      "type": "VariableDeclaration",
      "declarations": [
        {
          "type": "VariableDeclarator",
          "id": {
            "type": "Identifier",
            "name": "goo",
            "decorators": [],
            "loc": {
              "start": {
                "line": 4,
                "column": 5
              },
              "end": {
                "line": 4,
                "column": 8
              }
            }
          },
          "init": {
            "type": "TSTypeAssertion",
            "typeAnnotation": {
              "type": "TSTypeReference",
              "typeName": {
                "type": "Identifier",
                "name": "C",
                "decorators": [],
                "loc": {
                  "start": {
                    "line": 4,
                    "column": 12
                  },
                  "end": {
                    "line": 4,
                    "column": 13
                  }
                }
              },
              "loc": {
                "start": {
                  "line": 4,
                  "column": 12
                },
                "end": {
                  "line": 4,
                  "column": 13
                }
              }
            },
            "expression": {
              "type": "Identifier",
              "name": "foo",
              "decorators": [],
              "loc": {
                "start": {
                  "line": 4,
                  "column": 15
                },
                "end": {
                  "line": 4,
                  "column": 18
                }
              }
            },
            "loc": {
              "start": {
                "line": 4,
                "column": 11
              },
              "end": {
                "line": 4,
                "column": 19
              }
            }
          },
          "loc": {
            "start": {
              "line": 4,
              "column": 5
            },
            "end": {
              "line": 4,
              "column": 19
            }
          }
        }
      ],
      "kind": "var",
      "loc": {
        "start": {
          "line": 4,
          "column": 1
        },
        "end": {
          "line": 4,
          "column": 19
        }
      }
    }
  ],
  "loc": {
    "start": {
      "line": 1,
      "column": 1
    },
    "end": {
      "line": 4,
      "column": 19
    }
  }
}
