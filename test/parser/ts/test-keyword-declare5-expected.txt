{
  "type": "Program",
  "statements": [
    {
      "type": "TSDeclareFunction",
      "function": {
        "type": "ScriptFunction",
        "id": {
          "type": "Identifier",
          "name": "a",
          "decorators": [],
          "loc": {
            "start": {
              "line": 1,
              "column": 10
            },
            "end": {
              "line": 1,
              "column": 11
            }
          }
        },
        "generator": false,
        "async": false,
        "expression": false,
        "params": [],
        "loc": {
          "start": {
            "line": 1,
            "column": 1
          },
          "end": {
            "line": 1,
            "column": 14
          }
        }
      },
      "loc": {
        "start": {
          "line": 1,
          "column": 1
        },
        "end": {
          "line": 1,
          "column": 14
        }
      }
    },
    {
      "type": "TSDeclareFunction",
      "function": {
        "type": "ScriptFunction",
        "id": {
          "type": "Identifier",
          "name": "a",
          "decorators": [],
          "loc": {
            "start": {
              "line": 2,
              "column": 18
            },
            "end": {
              "line": 2,
              "column": 19
            }
          }
        },
        "generator": false,
        "async": false,
        "expression": false,
        "params": [],
        "declare": true,
        "loc": {
          "start": {
            "line": 2,
            "column": 9
          },
          "end": {
            "line": 2,
            "column": 22
          }
        }
      },
      "loc": {
        "start": {
          "line": 2,
          "column": 9
        },
        "end": {
          "line": 2,
          "column": 22
        }
      }
    }
  ],
  "loc": {
    "start": {
      "line": 1,
      "column": 1
    },
    "end": {
      "line": 2,
      "column": 22
    }
  }
}
